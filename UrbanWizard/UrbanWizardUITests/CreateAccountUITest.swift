//
//  CreateAccountUITest.swift
//  UrbanWizardUITests
//
//  Created by Will Saults on 11/18/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import XCTest
import Nimble

class CreateAccountUITest: XCTestCase {
    var app: XCUIApplication = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        AsyncDefaults.Timeout = 1
        continueAfterFailure = false
        app.launchArguments = ["UI-TESTING"]
        app.launch()
        app.tap()
    }

    func test_createAccount() {
        expect(self.app.pageIndicators.element(boundBy: 0).exists).toEventually(beTrue())
        
        app.buttons["Sign Up Now"].tap()
        expect(self.app.navigationBars.buttons["Back"].exists).toEventually(beTrue())
        app.navigationBars.buttons.element(boundBy: 0).tap() // Back button
        app.buttons["Sign Up Now"].tap()
        
//        expect(self.app.navigationBars.staticTexts["Create Account"].exists).to(beTrue())
//        expect(self.app.staticTexts["Create Account"].exists).to(beTrue())
        expect(self.app.staticTexts["Email Address"].exists).to(beFalse())
        app.textFields["Email Address"].tap()
        app.textFields["Email Address"].typeText("test@example.com")
        expect(self.app.staticTexts["Email Address"].exists).toEventually(beTrue())
        
        expect(self.app.staticTexts["Password"].exists).to(beFalse())
        let passwordField = app.secureTextFields["Password"]
        passwordField.tap()
        passwordField.typeText("Password1!")
        expect(passwordField.title).toEventuallyNot(equal("Password1!"))
        app.buttons["i1"].tap()
        // TODO: Check that password entry is visible
        app.buttons["i1"].tap()
        expect(self.app.staticTexts["Password"].exists).toEventually(beTrue())
        
        expect(self.app.staticTexts["Confirm Password"].exists).to(beFalse())
        let confirmPasswordField = app.secureTextFields["Confirm Password"]
        confirmPasswordField.tap()
        confirmPasswordField.typeText("Password1!")
        expect(confirmPasswordField.title).toEventuallyNot(equal("Password1!"))
        app.buttons["i2"].tap()
        // TODO: Check that password entry is visible
        app.buttons["i2"].tap()
        expect(self.app.staticTexts["Confirm Password"].exists).toEventually(beTrue())
        
        expect(self.app.staticTexts["Minimum eight characters"].exists).toEventually(beTrue())
        expect(self.app.staticTexts["Must include one number"].exists).toEventually(beTrue())
        expect(self.app.staticTexts["Must include an uppercase character"].exists).toEventually(beTrue())
        expect(self.app.staticTexts["Must include a lowercase character"].exists).toEventually(beTrue())
        expect(self.app.staticTexts["Must include a special character"].exists).toEventually(beTrue())
        
//        expect(self.app.staticTexts["Create Account"].exists).toEventually(beTrue())
        
        app.secureTextFields["Password"].tap()
        app.secureTextFields["Password"].typeText("Password1!#$%&()*+,-.:;<=>?@^_{|}[]/")
        app.secureTextFields["Confirm Password"].tap()
        app.secureTextFields["Confirm Password"].typeText("Password1!#$%&()*+,-.:;<=>?@^_{|}[]/")
        expect(self.app.buttons["Continue"].isEnabled).toEventually(beTrue())
        
        app.secureTextFields["Password"].tap()
        app.secureTextFields["Password"].typeText("Password1!")
        app.secureTextFields["Confirm Password"].tap()
        app.secureTextFields["Confirm Password"].typeText("Password1!")
        
        app.buttons["Continue"].tap()
        
        expect(self.app.staticTexts["Program Agreement"].exists).toEventually(beTrue())
        app.buttons["I agree"].tap()
        
//        expect(self.app.staticTexts["Home View"].exists).toEventually(beTrue())
    }
}
