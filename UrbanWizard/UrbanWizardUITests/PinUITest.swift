//
//  PinUITest.swift
//  UrbanWizardUITests
//
//  Created by Will Saults on 12/5/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import XCTest
import Nimble

class PinUITest: XCTestCase {
    var app: XCUIApplication = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        AsyncDefaults.Timeout = 1
        continueAfterFailure = false
        app.launchArguments = ["UI-TESTING", "isLoggedIn"]
        app.launch()
        app.tap()
    }
    
    func test_pin() {
        app.buttons["Show PIN"].tap()
        
        // Expect logout alert
        
        expect(self.app.staticTexts["Enter your PIN"].exists).toEventually(beTrue())
        
        // Tap the touchID button
        app.buttons["TouchID"].tap()
        
        app.buttons["1"].tap()
        app.buttons["2"].tap()
        app.buttons["3"].tap()
        app.buttons["4"].tap()
        
        // Tap delete 4 times
        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()
        
        app.buttons["5"].tap()
        app.buttons["6"].tap()
        app.buttons["7"].tap()
        app.buttons["8"].tap()
        
        // Tap delete 4 times
        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()
        
        app.buttons["9"].tap()
        app.buttons["0"].tap()
        app.buttons["9"].tap()
        app.buttons["0"].tap()
        
        // Expect the UI do dismiss
    }
}
