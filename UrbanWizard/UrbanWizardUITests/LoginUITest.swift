//
//  LoginUITest.swift
//  UrbanWizardUITests
//
//  Created by Will Saults on 11/28/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import XCTest
import Nimble

class LoginUITest: XCTestCase {
    var app: XCUIApplication = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        AsyncDefaults.Timeout = 1
        continueAfterFailure = false
        app.launchArguments = ["UI-TESTING"]
        app.launch()
        app.tap()
    }
    
    func test_login() {
        expect(self.app.pageIndicators.element(boundBy: 0).exists).toEventually(beTrue())
        
        app.buttons["or Log In"].tap()
        expect(self.app.navigationBars.buttons["Back"].exists).toEventually(beTrue())
        app.navigationBars.buttons.element(boundBy: 0).tap() // Back button
        app.buttons["or Log In"].tap()
        
//        expect(self.app.navigationBars.staticTexts["Login"].exists).to(beTrue())
//        expect(self.app.staticTexts["Login"].exists).toEventually(beTrue())
        app.textFields["Email Address"].tap()
        app.textFields["Email Address"].typeText("test@example.com")
        app.secureTextFields["Password"].tap()
        app.secureTextFields["Password"].typeText("Password1!")
        
        app.buttons["Login"].tap()
        
//        expect(self.app.staticTexts["Home View"].exists).toEventually(beTrue())
        
        let bitcoinAlert = self.app.alerts["Bitcoin"]
        expect(bitcoinAlert.exists).toEventually(beTrue())
        expect(bitcoinAlert.staticTexts["ID: bitcoin"].exists).toEventually(beTrue())
        bitcoinAlert.buttons["OK"].tap()
    }
}
