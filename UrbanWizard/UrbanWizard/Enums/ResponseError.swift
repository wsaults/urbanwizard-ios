//
//  ResponseError.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation

enum ResponseError: Error {
    static var errorMessage: String?
    
    case badRequest(String?)
    case conflict(String?)
    case timeout(String?)
    case unauthorized
    case notFound
    case internalSeverError
    case networkUnavailable
    case couldNotParseJSON
    case unknown
    
    func statusCode() -> Int {
        switch self {
        case .badRequest:
            return 422
        case .conflict:
            return 409
        case .timeout:
            return 408
        case .unauthorized:
            return 401
        case .notFound:
            return 404
        case .unknown:
            return 400
        case .internalSeverError:
            return 500
        case .networkUnavailable:
            return -1
        case .couldNotParseJSON:
            return -2
        }
    }
    
    func message() -> String {
        let message = ResponseError.errorMessage ?? ""
        ResponseError.errorMessage = nil
        return message
    }
    
    init(code: Int, error: String? = nil) {
        ResponseError.errorMessage = error
        
        switch code {
        case 422:
            self = .badRequest(error)
        case 409:
            self = .conflict(error)
        case 408:
            self = .timeout(error)
        case 401:
            self = .unauthorized
        case 404:
            self = .notFound
        case 500:
            self = .internalSeverError
        case -1:
            self = .networkUnavailable
        case -2:
            self = .couldNotParseJSON
        default:
            self = .unknown
        }
    }
}
