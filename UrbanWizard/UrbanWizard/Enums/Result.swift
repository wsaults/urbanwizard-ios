//
//  Result.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(ResponseError)
}
