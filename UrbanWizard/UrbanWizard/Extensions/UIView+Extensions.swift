//
//  UIView+Extensions.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/20/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

extension UIView {
    func addEndEditingTapGesture() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped)))
    }
    
    @objc func viewTapped() {
        endEditing(true)
    }
}
