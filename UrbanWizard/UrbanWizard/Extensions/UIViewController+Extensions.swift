//
//  UIViewController+Extensions.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/3/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit
import Freddy
import LocalAuthentication
import MBProgressHUD

extension UIViewController {
    func showAlert(_ title: String, message: String?) {
        DispatchQueue.main.async {
            self.hideHUD()
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showNetworkErrorAlert(_ statusCode: Int, message: String? = nil) {
        
        if let message = message, message.count > 0 {
            showAlert("", message: message)
            return
        }
        
        switch statusCode {
        case 401:
            showAlert("Session Expired", message: "Please sign out and try again.")
        case -1:
            showAlert("We're having trouble connecting.", message: "Please check your internet connection and try again.")
        default:
            showAlert("Oops, something went wrong.", message: "[Code: \(statusCode)]")
        }
    }
    
    func showHUD() {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func hideHUD() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}
