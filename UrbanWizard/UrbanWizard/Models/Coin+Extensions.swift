//
//  Coin+Extensions.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/10/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import Freddy
import CoreData

extension Coin {
    convenience init(context: NSManagedObjectContext, name: String) {
        self.init(context: context)
        
        self.name = name
    }
}

// MARK: - JSON

extension Coin {
    enum InvalidJsonError: Error {
        case badData
    }
    
    convenience init(context: NSManagedObjectContext, json value: JSON) throws {
        self.init(context: context)
        
        guard let name = try? value.getString(at: "name"), name.count > 0 else {
            context.delete(self)
            throw InvalidJsonError.badData
        }
        
        self.name = name
    }
}

extension Coin: JSONEncodable {
    public func toJSON() -> JSON {
        var jsonDictionary: [String:JSON] = [:]

        if let name = name {
            jsonDictionary["name"] = .string(name)
        }

        return .dictionary(jsonDictionary)
    }
}

