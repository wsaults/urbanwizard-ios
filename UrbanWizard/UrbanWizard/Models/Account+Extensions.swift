//
//  Account+Extensions.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import CoreData

extension Account {
    convenience init(context: NSManagedObjectContext, email: String) {
        self.init(context: context)
        
        self.email = email
    }
}
