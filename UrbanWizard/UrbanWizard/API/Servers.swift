//
//  Servers.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation

enum Servers {
    static var current = Servers.localhost // Change this to prod
    
    case prod
    case dev
    case uat
    case staging
    case localhost
    
    var components: URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = self.scheme
        urlComponents.path = self.path!
        
        let baseHost = ".urbanwizard.com"
        
        switch self {
        case .prod:
            urlComponents.host = "app\(baseHost)"
        case .dev:
            urlComponents.host = "dev\(baseHost)"
        case .uat:
            urlComponents.host = "uat\(baseHost)"
        case .staging:
            urlComponents.host = "stage\(baseHost)"
        case .localhost:
            urlComponents.host = "localhost"
            urlComponents.port = 8000
        }
        return urlComponents
    }
    
    fileprivate var scheme: String {
        switch self {
        case .localhost:
            return "http"
        default:
            return "https"
        }
    }
    
    fileprivate var path: String? {
        return "/api"
    }
}
