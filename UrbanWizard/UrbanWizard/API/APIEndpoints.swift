//
//  APIEndpoints.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import Freddy

enum APIEndpoints {
    
    // CREATE
    case createAccount
    case createSession
    case forgotPassword
    // READ
    case readTicker(coinId: String)
    case readSupportedCoins
    
    fileprivate var path: String {
        switch self {
        // MARK: - CREATE
        case .createAccount:
            return "/accounts"
        case .createSession:
            return "/sessions"
        case .forgotPassword:
            return "/accounts/password/new"
        // MARK: - READ
        case .readTicker(let id):
            return "/ticker/\(id)"
        case .readSupportedCoins:
            return "/supportedCoins"
        }
    }
    
    var URL: Foundation.URL {
        var urlComponents = Servers.current.components
        urlComponents.path = urlComponents.path + path
        
        switch self {
        case .forgotPassword:
            urlComponents.path = path
        default:
            break
        }
        
        return urlComponents.url!
    }
}
