//
//  HTTPClientManager.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import Freddy
import CoreData

typealias ResultStringObject = Result<[String: NSObject]>
typealias ResultStringAny = Result<[String: Any]>
typealias ResultJSON = Result<JSON>
typealias ResultJSONArray = Result<[JSON]>

class HTTPClientManager {
    var client = HTTPClient()
    
    // MARK: - Create
    
    func createAccount(email: String, password: String, confirmPassword: String, completion: @escaping (ResultStringAny) -> Void) {
        let url = APIEndpoints.createAccount.URL
        let body = JSON.dictionary([
            "account": .dictionary([
                "email": .string(email),
                "password": .string(password),
                "password_confirmation": .string(confirmPassword)
            ])
        ])

        client.post(url: url, body: body) {
            data, response, error in

            DispatchQueue.main.async {
                [unowned self] in
                
                if let _ = error {
                    completion(Result.failure(ResponseError.networkUnavailable))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    return
                }
                
                if httpResponse.successful {
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        let json = try JSON(data: data)
                        
                        let token = try json.getString(at: "auth_token")
                        
                        completion(Result.success(["account": ["email": email], "token": token]))
                    } catch {
                        completion(Result.failure(ResponseError.couldNotParseJSON))
                        return
                    }
                } else {
                    completion(Result.failure(self.parseError(data: data, response: httpResponse)))
                }
            }
        }
    }
    
    func createSession(email: String, password: String, completion: @escaping (ResultStringObject) -> Void) {
        let url = APIEndpoints.createSession.URL
        let body = JSON.dictionary([
            "session": .dictionary([
                "email": .string(email),
                "password": .string(password)
                ])
        ])
        
        client.post(url: url, body: body) {
            data, response, error in
            
            DispatchQueue.main.async {
                [unowned self] in
                
                if let _ = error {
                    completion(Result.failure(ResponseError.networkUnavailable))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    return
                }
                
                if httpResponse.successful {
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        let json = try JSON(data: data)
                        let authToken = try json.getString(at: "token")
                        
                        completion(Result.success(["token": authToken as NSObject]))
                    } catch {
                        completion(Result.failure(ResponseError.couldNotParseJSON))
                        return
                    }
                } else {
                    completion(Result.failure(self.parseError(data: data, response: httpResponse)))
                }
            }
        }
    }
    
    // MARK: - Read
    func readTickerFor(coin: String, completion: @escaping (ResultJSON) -> Void) {
        let url = APIEndpoints.readTicker(coinId: coin).URL
        
        client.get(url: url) {
            data, response, error in
            
            DispatchQueue.main.async {
                [unowned self] in
                
                if let _ = error {
                    completion(Result.failure(ResponseError.networkUnavailable))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    return
                }
                
                if httpResponse.successful {
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        let json = try JSON(data: data)
                        if let jsonCoin = try json.getArray().first {
                            completion(Result.success(jsonCoin))
                        }
                    } catch {
                        completion(Result.failure(ResponseError.couldNotParseJSON))
                        return
                    }
                } else {
                    completion(Result.failure(self.parseError(data: data, response: httpResponse)))
                }
            }
        }
    }
    
    func readSupportedCoins(completion: @escaping (ResultJSONArray) -> Void) {
        let url = APIEndpoints.readSupportedCoins.URL
        
        client.get(url: url) {
            data, response, error in
            
            DispatchQueue.main.async {
                [unowned self] in
                
                if let _ = error {
                    completion(Result.failure(ResponseError.networkUnavailable))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    return
                }
                
                if httpResponse.successful {
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        let json = try JSON(data: data)
                        completion(Result.success(try json.getArray()))
                    } catch {
                        completion(Result.failure(ResponseError.couldNotParseJSON))
                        return
                    }
                } else {
                    completion(Result.failure(self.parseError(data: data, response: httpResponse)))
                }
            }
        }
    }
    
    // MARK: - Update
    
    // MARK: - Delete
}

extension HTTPClientManager {
    func parseError(data: Data?, response: HTTPURLResponse) -> ResponseError {
        if response.statusCode == 409 {
            return ResponseError.conflict(nil)
        }
        
        guard let data = data, data.count > 0 else {
            return ResponseError(code: response.statusCode)
        }
        
        do {
            let json = try JSON(data: data)
            let error = try json.getArray(at: "errors").map(String.init).first
            return ResponseError(code: response.statusCode, error: error)
        } catch {
            do {
                let json = try JSON(data: data)
                let error = try json.getString(at: "error")
                return ResponseError(code: response.statusCode, error: error)
            } catch {
                return ResponseError.couldNotParseJSON
            }
        }
    }
}
