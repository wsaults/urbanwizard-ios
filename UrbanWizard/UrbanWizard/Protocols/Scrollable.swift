//
//  Scrollable.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/20/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit.UIViewController

@objc protocol KeyboardObserving {
    func keyboardWillChangeFrame(_ notification: Notification)
    func keyboardWillHide(_ notification: Notification)
}

protocol Scrollable: KeyboardObserving {
}

extension Scrollable where Self: UIViewController {
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
}
