//
//  AlertPresenting.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/3/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

protocol AlertPresenting {
    func present(_ title: String, message: String?, actionButtonTitle: String?, cancelButtonTitle: String?,
                 handler: (() -> Void)?, onTopOf presenter: UIViewController)
}
