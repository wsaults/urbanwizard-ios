//
//  CreateAccountViewModel.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/5/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class CreateAccountViewModel: NSObject {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailView: TextFieldAndLabelView!
    @IBOutlet weak var passwordView: TextFieldAndLabelView!
    @IBOutlet weak var confirmPasswordView: TextFieldAndLabelView!
    @IBOutlet weak var minimumEightCharactersView: RequirementLabelView!
    @IBOutlet weak var minimumOneNumberView: RequirementLabelView!
    @IBOutlet weak var minimumOneUpperCaseView: RequirementLabelView!
    @IBOutlet weak var minimumOneLowerCaseView: RequirementLabelView!
    @IBOutlet weak var minimumOneSpecialCharacterView: RequirementLabelView!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var createAccountButtonBottomContraint: NSLayoutConstraint!
    
    var clientManager = HTTPClientManager()
    var session = Session.shared
    var coreDataStack: CoreDataStack?
    
    func setup(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
    }
    
    func emailIsValid() -> Bool {
        guard let email = emailView.textField?.text else {
            return false
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: email) {
            emailView.isValid(false)
            return false
        }
        
        emailView.isValid(true)
        return true
    }
    
    func passwordIsValid() -> Bool {
        guard let password = passwordView?.textField.text else {
            return false
        }
        
        let minimumCountIsMet = password.count >= 8
        minimumEightCharactersView.requirement(isMet: minimumCountIsMet)
        
        if password.count > 64 {
            passwordView.isValid(false)
            return false
        }
        
        let capitalTestIsMet = containsAnUpperCaseLetter(password: password)
        let lowerCaseTestIsMet = containsALowerCaseLetter(password: password)
        let numberTestIsMet = containsANumber(password: password)
        let specialCharacterTestIsMet = containsASpecialCharacter(password: password)
        
        let forbiddenCharacterSet = CharacterSet(charactersIn: "\\` '\"")
        if password.rangeOfCharacter(from: forbiddenCharacterSet) != nil {
            passwordView.isValid(false)
            return false
        }
        
        let numberOfMetTests = [minimumCountIsMet, capitalTestIsMet, lowerCaseTestIsMet, numberTestIsMet, specialCharacterTestIsMet].filter{$0}.count
        passwordView.valid(0.2 * CGFloat(numberOfMetTests))
        
        return minimumCountIsMet && capitalTestIsMet && lowerCaseTestIsMet && numberTestIsMet && specialCharacterTestIsMet
    }
    
    func doPasswordsMatch() -> Bool {
        guard let passwordText = passwordView.textField?.text,
            let confirmPasswordText = confirmPasswordView.textField?.text else {
                return false
        }
        
        if confirmPasswordText.count == 0 || passwordText != confirmPasswordText {
            confirmPasswordView.isValid(false)
            return false
        }
        
        confirmPasswordView.isValid(true)
        return true
    }
    
    func validateInputFields() -> Bool {
        let isEmailValid = emailIsValid()
        let isPasswordValid = passwordIsValid()
        let isMatchingPassword = doPasswordsMatch()
        
        return isEmailValid && isPasswordValid && isMatchingPassword
    }
    
    // MARK: - API
    
    func createAccount(forViewController viewController: CreateAccountViewController, completion: @escaping (() -> Void)) {
        guard let email = emailView.textField?.text,
            let password = passwordView.textField?.text,
            let confirmPassword = confirmPasswordView.textField?.text else {
                return
        }
        
        viewController.showHUD()
        clientManager.createAccount(email: email, password: password, confirmPassword: confirmPassword) {
            [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            
            viewController.hideHUD()
            switch result {
            case .success(let accountDictionary):
                if let account = accountDictionary["account"] as? NSDictionary, let email = account["email"] as? String, let context = self?.coreDataStack?.mainContext {
                    let _ = Account(context: context, email: email)
                    self?.coreDataStack?.saveContext()
                }
                
                if let token = accountDictionary["token"] as? String {
                    strongSelf.session.authToken = token
                }
                
                completion()
            case .failure(let error):
                switch error {
                case .conflict(_):
                    viewController.showExistingAccountAlert()
                default:
                    viewController.showNetworkErrorAlert(error.statusCode(), message: error.message())
                    break
                }
            }
        }
    }
    
    // MARK: - Private
    
    private func containsAnUpperCaseLetter(password: String) -> Bool {
        // Contains an uppercase letter
        let capitalLetterRegEx = ".*[A-Z]+.*"
        let capitalTest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalTestIsMet = capitalTest.evaluate(with: password)
        minimumOneUpperCaseView.requirement(isMet: capitalTestIsMet)
        return capitalTestIsMet
    }
    
    private func containsALowerCaseLetter(password: String) -> Bool {
        // Contains a lowercase letter
        let lowerCaseLetterRegEx = ".*[a-z]+.*"
        let lowerCaseTest = NSPredicate(format: "SELF MATCHES %@", lowerCaseLetterRegEx)
        let lowerCaseTestIsMet = lowerCaseTest.evaluate(with: password)
        minimumOneLowerCaseView.requirement(isMet: lowerCaseTestIsMet)
        return lowerCaseTestIsMet
    }
    
    private func containsANumber(password: String) -> Bool {
        // Contains a number
        let numberRegEx = ".*[0-9]+.*"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let numberTestIsMet = numberTest.evaluate(with: password)
        minimumOneNumberView.requirement(isMet: numberTestIsMet)
        return numberTestIsMet
    }
    
    private func containsASpecialCharacter(password: String) -> Bool {
        // Contains a special character
        let specialCharacterRegEx = ".*[!#$%&()*+,-.:;<=>?@^_{|}\\[\\]\\/]+.*"
        let specialCharacterTest = NSPredicate(format: "SELF MATCHES %@", specialCharacterRegEx)
        let specialCharacterTestIsMet = specialCharacterTest.evaluate(with: password)
        minimumOneSpecialCharacterView.requirement(isMet: specialCharacterTestIsMet)
        return specialCharacterTestIsMet
    }
}
