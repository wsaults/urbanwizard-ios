//
//  PinViewModel.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/5/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class PinViewModel: NSObject {
    
    @IBOutlet weak var pinLabel: UILabel!
    @IBOutlet weak var pinEntryLabel: UILabel!
    
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var zeroButton: UIButton!
    
    @IBOutlet weak var deviceAuthButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    var pinCode = ""
    
    func pinIsValid() -> Bool {
        guard pinCode.count == 4 else {
            return false
        }
        
        guard let _ = Int(pinCode) else {
            return false
        }
        
        return true
    }
    
    @IBAction func pinKeyPressed(_ sender: UIButton) {
        if pinCode.count >= 4 { return }
        
        var currentPin = pinCode
        currentPin += sender.titleLabel?.text ?? ""
        pinCode = currentPin
        
        updatePinEntryLabel(withCode: pinCode)
    }
    
    private func updatePinEntryLabel(withCode code: String) {
        pinEntryLabel.text = String(repeating: "●", count: code.count)
    }
}
