//
//  HomeViewModel.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/10/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit
import CoreData

class HomeViewModel: NSObject {
    
    @IBOutlet weak var tableView: UITableView!
    
    var clientManager = HTTPClientManager()
    var coreDataStack: CoreDataStack!
    lazy var fetchedResultsController: NSFetchedResultsController<Coin> = {
        let fetchRequest: NSFetchRequest<Coin> = Coin.fetchRequest()
        let nameSort = NSSortDescriptor(key: #keyPath(Coin.name), ascending: true)
        fetchRequest.sortDescriptors = [nameSort]
        
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: coreDataStack.mainContext,
            sectionNameKeyPath: nil,
            cacheName: "coin")
        
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    var receivedCoin = Coin()
    
    func setup(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CellIdentifier")
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Fetching error: \(error), \(error.userInfo)")
        }
    }
    
    // MARK: - Actions
    
    @IBAction func setupPortfolio(_ sender: UIButton) {
        readSupportedCoins {
            print("readSupportedCoins completed")
        }
    }
    
    // MARK: - API
    
//    func readCoin(id: String, completion: @escaping (() -> Void)) {
//        
//        clientManager.readTickerFor(coin: "bitcoin") {
//            [unowned self] (result) in
//            
//            switch result {
//            case .success(let coinJson):
//                let context = self.coreDataStack.mainContext
//                
//                do {
//                    let coin = try Coin(context: context, json: coinJson)
//                    self.receivedCoin = coin
//                } catch let error as NSError {
//                    print("Error: \(error), \(error.userInfo)")
//                }
//                completion()
//            default:
//                break
//            }
//        }
//    }
    
    func readSupportedCoins(completion: @escaping (() -> Void)) {
        clientManager.readSupportedCoins {
            [unowned self] (result) in
            
            switch result {
            case .success(let coinJsonArray):
                let context = self.coreDataStack.mainContext
                
                for json in coinJsonArray {
                    do {
                        let _ = try Coin(context: context, json: json)
                        self.coreDataStack.saveContext()
                    } catch let error as NSError {
                        print("Error: \(error), \(error.userInfo)")
                    }
                }
                completion()
            default:
                break
            }
        }
    }
}

// MARK: = Internal
extension HomeViewModel {
    func configure(cell: UITableViewCell, for indexPath: IndexPath) {
        // TOOD: Bind to custom cell
        
        let coin = fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = coin.name
    }
}

// MARK: - UITableViewDelegate

extension HomeViewModel: UITableViewDelegate {
    // TODO: handle cell selection
}

// MARK: - UITableViewDatasource

extension HomeViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionInfo = fetchedResultsController.sections?[section] else {
            return 0
        }
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        configure(cell: cell, for: indexPath)
        
        return cell
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension HomeViewModel: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // TODO: temporary
        tableView.reloadData()
    }
}
