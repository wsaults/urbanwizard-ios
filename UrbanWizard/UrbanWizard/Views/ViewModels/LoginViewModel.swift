//
//  LoginViewModel.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/5/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit
import CoreData

class LoginViewModel: NSObject {
    
    @IBOutlet var emailView: TextFieldAndLabelView!
    @IBOutlet var passwordView: TextFieldAndLabelView!
    @IBOutlet var forgotPasswordButton: UIButton!
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var signInButtonBottomConstraint: NSLayoutConstraint!
    
    var session = Session.shared
    var coreDataStack: CoreDataStack?
    var clientManager = HTTPClientManager()
    
    func setup(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
    }
    
    func validateInputFields() -> Bool {
        return emailIsValid() && passwordIsValid()
    }
    
    func emailIsValid() -> Bool {
        guard let email = emailView?.textField.text else {
            return false
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: email) {
            emailView.isValid(false)
            return false
        }
        
        emailView.isValid(true)
        return true
    }
    
    func passwordIsValid() -> Bool {
        guard let password = passwordView?.textField.text else {
            return false
        }
        
        let isValid = password.count >= 8
        passwordView.isValid(isValid)
        
        return isValid
    }
    
    // MARK: - API
    
    func createSession(email: String, password: String, forViewController viewController: LoginViewController, completion: @escaping () -> Void) {
        viewController.showHUD()
        clientManager.createSession(email: email, password: password) {
            [weak self] (result) in
            
            guard let strongSelf = self else {
                return
            }
            
            viewController.hideHUD()
            
            switch result {
            case .success(let authToken):
                strongSelf.session.authToken = authToken["token"] as? String
                
                guard let context = self?.coreDataStack?.mainContext else {
                    // TODO: Handle bad context
                    return
                }

                let _ = Account(context: context, email: email)
                self?.coreDataStack?.saveContext()
                
                completion()
            case .failure(let error):
                viewController.showNetworkErrorAlert(error.statusCode(), message: error.message())
            }
        }
        
        return
    }
}
