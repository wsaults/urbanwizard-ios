//
//  RequirementLabelView.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/9/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class RequirementLabelView: UIView {
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var label: UILabel!
    
    let requirementMetColor = UIColor.green
    let requirementNotMetColor = UIColor.black
    
    func requirement(isMet enabled: Bool) {
        label?.textColor = requirementMetColor(isMet: enabled)
        dotView?.backgroundColor = requirementMetColor(isMet: enabled)
    }
    
    private func requirementMetColor(isMet: Bool) -> UIColor {
        return isMet ? requirementMetColor : requirementNotMetColor
    }
}
