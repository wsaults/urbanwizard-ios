//
//  WelcomeViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 11/16/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var logInButton: UIButton!
    var coreDataStack: CoreDataStack!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCreateAccountViewController",
            let createAccountViewController = segue.destination as? CreateAccountViewController {
            createAccountViewController.coreDataStack = coreDataStack
            return
        }
        
        if segue.identifier == "toLoginViewController",
            let loginViewController = segue.destination as? LoginViewController {
            loginViewController.coreDataStack = coreDataStack
            return
        }
    }
}
