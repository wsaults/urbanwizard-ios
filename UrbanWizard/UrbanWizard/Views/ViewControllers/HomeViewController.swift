//
//  HomeViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 11/16/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var viewModel: HomeViewModel!
    var coreDataStack: CoreDataStack!
    
    var alertPresenter: AlertPresenting = AlertPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.setup(coreDataStack: coreDataStack)
        
//        viewModel.readCoin(id: "bitcoin") {
//            [unowned self] in
//            self.showCoinAlert()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Alerts
    
    fileprivate func showCoinAlert() {
        alertPresenter.present(viewModel?.receivedCoin.name ?? "",
                               message: "ID: \(viewModel?.receivedCoin.name ?? "")",
                               actionButtonTitle: nil,
                               cancelButtonTitle: "OK",
                               handler: nil,
                               onTopOf: self)
    }
}
