//
//  LoginViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 11/16/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var viewModel: LoginViewModel!
    var coreDataStack: CoreDataStack!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let coreDataStack = coreDataStack {
            viewModel?.setup(coreDataStack: coreDataStack)
        }
        
        view.addEndEditingTapGesture()
        addKeyboardObservers()
        
        setupTextField(viewModel.emailView.textField)
        setupTextField(viewModel.passwordView.textField)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func textFieldDidChange() {
        updateSignInButtonState()
    }
    
    // MARK: - Private
    
    fileprivate func setupTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    fileprivate func updateSignInButtonState() {
        viewModel.signInButton.isEnabled = viewModel.validateInputFields()
    }
    
    // MARK: - Actions
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        createSessionFromTextFields {
            [unowned self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            homeViewController.coreDataStack = self.coreDataStack
            self.present(homeViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func createSessionFromTextFields(_ completion: @escaping () -> Void) {
        guard let email = viewModel.emailView.textField.text,
            let password = viewModel.passwordView.textField.text else {
                return
        }
        
        viewModel.createSession(email: email, password: password, forViewController: self, completion: completion)
    }
}

// MARK: - UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

// MARK: - Scrollable

extension LoginViewController: Scrollable {
    func keyboardWillChangeFrame(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt
        
        if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
            self.viewModel.signInButtonBottomConstraint.constant = 0.0
        } else {
            self.viewModel.signInButtonBottomConstraint.constant = -(endFrame?.size.height ?? 0.0)
        }
        
        let options = UIViewAnimationOptions(rawValue: curve ?? 0 << 16)
        UIView.animate(withDuration: duration ?? 0.25,
                       delay: TimeInterval(0),
                       options: options,
                       animations: { self.view.layoutIfNeeded() },
                       completion: nil)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber
        UIView.animate(withDuration: TimeInterval(duration?.floatValue ?? 0.25), animations: {
            [unowned self] in
            self.viewModel.signInButtonBottomConstraint.constant = 0
        })
    }
}
