//
//  PlaidLinkViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/4/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit
import LinkKit

class PlaidLinkViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Actions
    
    @IBAction func launchPlaid(_ sender: Any) {
        let linkConfiguration = LinkConfiguration.shared
        linkConfiguration.launch(forViewController: self)
    }
}
