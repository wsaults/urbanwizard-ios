//
//  CreateAccountViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 11/16/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController {
    
    @IBOutlet weak var viewModel: CreateAccountViewModel!
    
    var alertPresenter: AlertPresenting = AlertPresenter()
    var coreDataStack: CoreDataStack!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let coreDataStack = coreDataStack {
            viewModel?.setup(coreDataStack: coreDataStack)
        }
        
        view.addEndEditingTapGesture()
        addKeyboardObservers()
        setupTextFields()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func textFieldDidChange() {
        updateCreateAccountButtonState()
    }
    
    // MARK: - Actions
    
    @IBAction func switchDidChange(_ sender: Any) {
        updateCreateAccountButtonState()
    }
    
    @IBAction func createAccountButtonTapped(_ sender: Any) {
        guard viewModel.validateInputFields() else {
            return
        }
        
        viewModel.createAccount(forViewController: self) {
            [unowned self] in
            let storyboard = UIStoryboard(name: "Signup", bundle: nil)
            let termsViewController = storyboard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
            self.present(termsViewController, animated: true)
        }
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private
    
    fileprivate func updateCreateAccountButtonState() {
        viewModel.createAccountButton.isEnabled = viewModel.validateInputFields()
    }
    
    fileprivate func setupTextFields() {
        setupTextField(viewModel.emailView.textField)
        setupTextField(viewModel.passwordView.textField)
        setupTextField(viewModel.confirmPasswordView.textField)
    }
    
    fileprivate func setupTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    // MARK: - Alerts
    
    fileprivate func showConfirmPasswordsMatchAlert() {
        alertPresenter.present("Passwords do not match",
                               message: "Please re-enter and confirm your password",
                               actionButtonTitle: nil,
                               cancelButtonTitle: "OK",
                               handler: nil,
                               onTopOf: self)
    }
    
    fileprivate func showInvalidEmailAlert() {
        alertPresenter.present("Invalid Email",
                               message: "Please enter a valid email address.",
                               actionButtonTitle: nil,
                               cancelButtonTitle: "OK",
                               handler: nil,
                               onTopOf: self)
    }
    
    func showExistingAccountAlert() {
        alertPresenter.present("This account already exists",
                               message: "The email address you have entered is already registered",
                               actionButtonTitle: "Sign In",
                               cancelButtonTitle: "Cancel",
                               handler: { /* Show sign in */ },
                               onTopOf: self)
    }
}

// MARK: - UITextFieldDelegate

extension CreateAccountViewController: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150), execute: {
            [unowned self] in
            self.updateCreateAccountButtonState()
        })
        
        return true
    }
}

// MARK: - Scrollable

extension CreateAccountViewController: Scrollable {
    func keyboardWillChangeFrame(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt
        
        if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
            self.viewModel.createAccountButtonBottomContraint.constant = 0.0
        } else {
            self.viewModel.createAccountButtonBottomContraint.constant = endFrame?.size.height ?? 0.0
        }
        
        let options = UIViewAnimationOptions(rawValue: curve ?? 0 << 16)
        UIView.animate(withDuration: duration ?? 0.25,
                       delay: TimeInterval(0),
                       options: options,
                       animations: { self.view.layoutIfNeeded() },
                       completion: nil)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber
        UIView.animate(withDuration: TimeInterval(duration?.floatValue ?? 0.25), animations: {
            [unowned self] in
            self.viewModel.createAccountButtonBottomContraint.constant = 0
        })
    }
}
