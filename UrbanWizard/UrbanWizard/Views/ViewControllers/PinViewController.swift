//
//  PinViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/5/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class PinViewController: UIViewController {
    
    @IBOutlet weak var viewModel: PinViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
