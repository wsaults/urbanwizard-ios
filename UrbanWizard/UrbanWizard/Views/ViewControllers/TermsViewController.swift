//
//  TermsViewController.swift
//  UrbanWizard
//
//  Created by Will Saults on 11/16/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {
    
    @IBOutlet var agreeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Actions
    
    @IBAction func agreeButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.present(homeViewController, animated: true)
    }
}
