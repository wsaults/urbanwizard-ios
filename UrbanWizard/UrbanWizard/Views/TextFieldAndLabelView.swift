//
//  TextFieldAndLabelView.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/6/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class TextFieldAndLabelView: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var validIndicator: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    let validColor = UIColor.green
    let invalidColor = UIColor.red
    let neutralColor = UIColor.gray
    var maxWidth = CGFloat()
    
    @IBAction func textDidChange(sender: UITextField) {
        if let isEmpty = sender.text?.isEmpty, isEmpty {
            label.text = ""
        } else {
            label.text = sender.placeholder
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        maxWidth = widthConstraint.constant
    }
    
    func isValid(_ valid: Bool) {
        validIndicator.backgroundColor = valid ? validColor : invalidColor
        widthConstraint.constant = maxWidth
    }
    
    func valid(_ percent: CGFloat) {
        validIndicator.backgroundColor = validColor
        widthConstraint.constant = maxWidth * percent
    }
}

class SecureTextFieldAndLabelView: TextFieldAndLabelView {
    @IBOutlet weak var toggleSecureEntryButton: UIButton!
    
    @IBAction func toggleSecureEntry() {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        reWritePassword()
    }
    
    // Seems hacky but this keeps there from being
    // whitespace after the password when you toggle secure entry.
    private func reWritePassword() {
        let password = textField.text
        textField.text = ""
        textField.text = password
    }
}
