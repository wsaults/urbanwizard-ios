//
//  AlertPresenter.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/3/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit

class AlertPresenter: AlertPresenting {
    func present(_ title: String, message: String?, actionButtonTitle: String?, cancelButtonTitle: String? = "OK",
                 handler: (() -> Void)?, onTopOf presentingViewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let cancelText = cancelButtonTitle, cancelText.count > 0 {
            let cancelAction = UIAlertAction(title: cancelText, style: .cancel, handler: nil)
            alert.addAction(cancelAction)
        }
        
        if let actionButtonTitle = actionButtonTitle {
            let settingsAction = UIAlertAction(title: actionButtonTitle, style: .default) {
                _ in
                if let handler = handler {
                    handler()
                }
            }
            alert.addAction(settingsAction)
        }
        
        presentingViewController.present(alert, animated: true, completion: nil)
    }
}
