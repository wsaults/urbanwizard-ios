//
//  Session.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation

class Session {
    static let shared = Session()
    
    var authToken: String?
    
    func clear() {
        authToken = nil
    }
    
    var isLoggedIn: Bool {
        guard let authToken = authToken else {
            return false
        }

        return !authToken.isEmpty
    }
}
