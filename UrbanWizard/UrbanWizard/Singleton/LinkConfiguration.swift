//
//  LinkConfiguration.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/4/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import LinkKit

class LinkConfiguration: NSObject {
    static let shared = LinkConfiguration()
    
    var linkConfiguration: PLKConfiguration?
    var environment: PLKEnvironment?
    var presentingViewController: UIViewController?
    
    var key: String {
        if let env = environment {
            switch env {
            case .sandbox:
                return "00435087b962e76373d2705031b2c7"
            case .development:
                return ""
            case .production:
                return ""
            case .tartan:
                return ""
            }
        }
        return ""
    }
    
    func setup(environment env: PLKEnvironment) {
        environment = env
        linkConfiguration = PLKConfiguration(key: key, env: env, product: .auth)
        guard let configuration = linkConfiguration else {
            return
        }
        
        configuration.clientName = "Link Demo"
        PLKPlaidLink.setup(with: configuration) {
            (success, error) in
            
            if success {
                NSLog("Plaid link setup was successful")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PLDPlaidLinkSetupFinished"), object: self)
            }
            else if let error = error {
                NSLog("Unable to setup Plaid Link due to: \(error.localizedDescription)")
            }
            else {
                NSLog("Unable to setup Plaid Link")
            }
        }
    }
    
    func launch(forViewController viewController: UIViewController) {
        presentingViewController = viewController
        guard let configuration = linkConfiguration else {
            return
        }
        
        let linkViewController = PLKPlaidLinkViewController(configuration: configuration, delegate: self)
        if(UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        viewController.present(linkViewController, animated: true)
    }
    
    // MARK: Delegate handlers
    
    func handleSuccessWithToken(_ publicToken: String, metadata: [String : Any]?) {
        
    }
    
    func handleError(_ error: Error?, metadata: [String : Any]?) {
        
    }
    
    func handleExitWithMetadata(_ metadata: [String : Any]?) {
        
    }
}

extension LinkConfiguration: PLKPlaidLinkViewDelegate {
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?) {
        presentingViewController?.dismiss(animated: true) {
            [unowned self] in
            NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
            self.handleSuccessWithToken(publicToken, metadata: metadata)
            self.presentingViewController = nil
        }
    }
    
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?) {
        presentingViewController?.dismiss(animated: true) {
            [unowned self] in
            if let error = error {
                NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
                self.handleError(error, metadata: metadata)
            }
            else {
                NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
                self.handleExitWithMetadata(metadata)
            }
            self.presentingViewController = nil
        }
    }
}
