//
//  Global.swift
//  UrbanWizard
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation

func equals<T:Equatable>(_ lhs: T?, rhs: T?) -> Bool {
    var isEqual = false
    if let lhs = lhs, let rhs = rhs {
        isEqual = lhs == rhs
    } else if lhs == nil && rhs == nil {
        isEqual = true
    }
    return isEqual
}
