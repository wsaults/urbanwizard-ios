require 'sinatra'
require 'json'

post '/api/accounts' do
    payload = JSON.parse request.body.read
    account = payload['account']
    
    status 201
    body = {
        "account"=>"http://localhost:8000/api/accounts/1",
        "auth_token"=>"1234"
    }.to_json
end

post '/api/sessions' do
    payload = JSON.parse request.body.read
    session = payload['session']

    if session['password'] == 'Password1!'
        status 200
        body = {
            "token"=>"1234"
        }.to_json
    end
end

get '/api/ticker/:id' do
    coin_id = params[:id]
    
    status 200
    body = [
        {
            "id"=>"bitcoin",
            "name"=>"Bitcoin"
        }
    ].to_json
end

get '/api/supportedCoins' do
    
    status 200
    body = [
    {"name"=>"bitcoin"},
    {"name"=>"litecoin"},
    {"name"=>"ripple"}
    ].to_json
end
