//
//  SessionSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble

@testable import UrbanWizard

class SessionSpec: QuickSpec {
    override func spec() {
        var subject: Session!
        
        beforeEach {
            subject = Session()
        }
        
        describe("Session") {
            describe("isLoggedIn") {
                it("returns true if there is an authToken") {
                    subject.authToken = "whatever"
                    
                    expect(subject.isLoggedIn).to(beTrue())
                }
                
                it("returns false if thers is not authToken") {
                    subject.authToken = nil
                    
                    expect(subject.isLoggedIn).to(beFalse())
                }
            }
        }
        
        describe("clearSession") {
            beforeEach {
                subject.authToken = "whatever"
                
                subject.clear()
            }
            
            it("clears authToken") {
                expect(subject.authToken).to(beNil())
            }
        }
    }
}
