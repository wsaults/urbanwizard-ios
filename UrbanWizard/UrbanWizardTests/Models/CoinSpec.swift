//
//  CoinSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/10/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import Freddy
import CoreData

@testable import UrbanWizard

class CoinSpec: QuickSpec {
    override func spec() {
        var coreDataStack: CoreDataStack!
        
        beforeEach {
            coreDataStack = MockCoreDataStack()
        }
        
        describe("Coin") {
            var mainContext: NSManagedObjectContext!
            
            beforeEach {
                mainContext = coreDataStack.mainContext
            }
            
            it("is created with a name matching bitcoin") {
                let coin = Coin(context: mainContext, name: "bitcoin")
                
                expect(coin.name).to(equal("bitcoin"))
            }
            
            it("is saved into the context") {
                let _ = Coin(context: mainContext, name: "bitcoin")
                
                mainContext.perform {
                    coreDataStack.saveContext()
                }
                
                var count = 0
                do {
                    let fetchRequest: NSFetchRequest<Coin> = Coin.fetchRequest()
                    count = try mainContext.count(for: fetchRequest)
                } catch let error as NSError {
                    print("Error \(error), \(error.userInfo)")
                }
                
                expect(count).toEventually(equal(1), timeout: 1)
            }
            
            it("contains two saved into the context") {
                let _ = Coin(context: mainContext, name: "bitcoin")
                let _ = Coin(context: mainContext, name: "ripple")
                
                mainContext.perform {
                    coreDataStack.saveContext()
                }
                
                var count = 0
                do {
                    let fetchRequest: NSFetchRequest<Coin> = Coin.fetchRequest()
                    count = try mainContext.count(for: fetchRequest)
                } catch let error as NSError {
                    print("Error \(error), \(error.userInfo)")
                }
                
                expect(count).toEventually(equal(2), timeout: 1)
            }
            
            
            describe("JSON") {
                var jsonDictionary: JSON!
                var coin: Coin!

                beforeEach {
                    coin = Coin(context: mainContext, name: "Bitcoin")

                    let dictionary: [String:JSON] = [
                        "name": .string("Bitcoin")
                    ]
                    jsonDictionary = JSON.dictionary(dictionary)
                }

                describe("JSONEncodable") {
                    it("toJSON should encode coin to matching json") {
                        expect(coin.toJSON()).to(equal(jsonDictionary))
                    }
                }

                describe("JSONDecodable") {
                    context("success") {
                        it("coin init with JSON should match") {
                            let coin2 = try! Coin(context: mainContext, json: jsonDictionary)
                            expect(coin.name).to(equal(coin2.name))
                        }
                    }

                    context("failure") {
                        it("should error when name is nil") {
                            let badDictionary: [String:JSON] = [
                                "name": nil
                            ]
                            let badJson = JSON.dictionary(badDictionary)
                            
                            expect { try Coin(context: mainContext, json: badJson) }.to(throwError())
                        }
                        
                        it("should error when name is empty") {
                            let badDictionary: [String:JSON] = [
                                "name": .string("")
                            ]
                            let badJson = JSON.dictionary(badDictionary)
                            
                            expect { try Coin(context: mainContext, json: badJson) }.to(throwError())
                        }
                    }
                }
            }
        }
    }
}
