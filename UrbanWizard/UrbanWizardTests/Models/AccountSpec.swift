//
//  AccountSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import Freddy
import CoreData

@testable import UrbanWizard

class AccountSpec: QuickSpec {
    override func spec() {
        var coreDataStack: CoreDataStack!
        
        beforeEach {
            coreDataStack = MockCoreDataStack()
        }
        
        describe("Account") {
            var mainContext: NSManagedObjectContext!
            
            beforeEach {
                mainContext = coreDataStack.mainContext
            }
            
            it("is created with an email matching test@example.com") {
                let account = Account(context: mainContext, email: "test@example.com")
                
                expect(account.email).to(equal("test@example.com"))
            }
            
            it("is saved into the context") {
                let _ = Account(context: mainContext, email: "test@example.com")
                
                mainContext.perform {
                    coreDataStack.saveContext()
                }
                
                var count = 0
                do {
                    let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
                    count = try mainContext.count(for: fetchRequest)
                } catch let error as NSError {
                    print("Error \(error), \(error.userInfo)")
                }
                
                expect(count).toEventually(equal(1), timeout: 1)
            }
            
            it("contains two saved into the context") {
                let _ = Account(context: mainContext, email: "test@example.com")
                let _ = Account(context: mainContext, email: "test2@example.com")
                
                mainContext.perform {
                    coreDataStack.saveContext()
                }
                
                var count = 0
                do {
                    let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
                    count = try mainContext.count(for: fetchRequest)
                } catch let error as NSError {
                    print("Error \(error), \(error.userInfo)")
                }
                
                expect(count).toEventually(equal(2), timeout: 1)
            }
        }
    }
}
