//
//  HTTPClientManagerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import Freddy
import CoreData

@testable import UrbanWizard

func ==<Value:Equatable, Key>(lhs: [Key: [Value]], rhs: [Key: [Value]]) -> Bool {
    if lhs.count != rhs.count {
        return false
    }
    
    for (key, lhsArray) in lhs {
        guard let rhsArray = rhs[key], lhsArray == rhsArray else {
            return false
        }
    }
    
    return true
}

class HTTPClientManagerSpec: QuickSpec {
    func resultHasError<T>(_ result: Result<T>, responseError: ResponseError) -> Bool {
        switch result {
        case .failure(let error):
            switch error {
            case .badRequest(let resultError):
                switch responseError {
                case .badRequest(let responseError):
                    if resultError == nil && responseError == nil {
                        return true
                    }
                    
                    if let resultError = resultError, let responseError = responseError {
                        return resultError == responseError
                    }
                default:
                    break
                }
            case .conflict(let resultError):
                switch responseError {
                case.conflict(let responseError):
                    if resultError == nil && responseError == nil {
                        return true
                    }
                    
                    if let resultError = resultError, let responseError = responseError {
                        return resultError == responseError
                    }
                default:
                    break
                }
            case .timeout(let resultError):
                switch responseError {
                case .timeout(let responseError):
                    if resultError == nil && responseError == nil {
                        return true
                    }
                    
                    if let resultError = resultError, let responseError = responseError {
                        return resultError == responseError
                    }
                default:
                    break
                }
            case .notFound:
                switch responseError {
                case .notFound:
                    return true
                default:
                    break
                }
            case .unauthorized:
                switch responseError {
                case .unauthorized:
                    return true
                default:
                    break
                }
            case .unknown:
                switch responseError {
                case .unknown:
                    return true
                default:
                    break
                }
            case .internalSeverError:
                switch responseError {
                case .networkUnavailable:
                    return true
                default:
                    break
                }
            case .networkUnavailable:
                switch responseError {
                case .networkUnavailable:
                    return true
                default:
                    break
                }
            case .couldNotParseJSON:
                switch responseError {
                case .couldNotParseJSON:
                    return true
                default:
                    break
                }
            }
        default:
            break
        }
        
        return false
    }
    
    override func spec() {
        var subject: HTTPClientManager!
        var mockClient: MockHTTPClient!
        var url: URL!
        var mockCoreDataStack: MockCoreDataStack!
        
        describe("HTTPClientManager") {
            beforeEach {
                mockCoreDataStack = MockCoreDataStack()
                subject = HTTPClientManager()
                mockClient = MockHTTPClient()
                subject.client = mockClient
                url = URL(string: "http://dev.urbanwizard.com")
            }
            
            describe("CREATE") {
                describe("createAccount") {
                    context("success") {
                        it("has matchign URL") {
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: { (result) in })
                            expect(mockClient.sentURL).to(equal("http://localhost:8000/api/accounts"))
                        }
                        
                        it("sent body should match") {
                            let expectedJSON = JSON.dictionary([
                                "account": .dictionary([
                                    "email": .string("test@example.com"),
                                        "password": .string("password"),
                                        "password_confirmation": .string("password")])
                                    ])
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: { _ in })
                            expect(mockClient.sentBody).to(equal(expectedJSON))
                        }
                        
                        it("completion handler is run with successful result") {
                            let responseJSON = JSON.dictionary([
                                "account": "http://localhost:8000/api/accounts/99",
                                "auth_token": "1234"
                                ])
                            mockClient.nextResponseData = try! responseJSON.serialize()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 201, httpVersion: nil, headerFields: nil)
                            var receivedAccount: [String: Any]?
                            let mockCompletion: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                switch result {
                                case .success(let accountResult):
                                    receivedAccount = accountResult["account"] as? Dictionary
                                default:
                                    break
                                }
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            
                            let mainContext = mockCoreDataStack.mainContext
                            let expectedAccont = Account(context: mainContext, email: "test@example.com")
                            
                            expect(receivedAccount!["email"] as? String).toEventually(equal(expectedAccont.email), timeout: 1)
                        }
                        
                        it("completion handler is run with successful token 1234") {
                            let responseJSON = JSON.dictionary([
                                "account": "http://localhost:8000/api/accounts/99",
                                "auth_token": "1234"
                                ])
                            
                            mockClient.nextResponseData = try! responseJSON.serialize()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 201, httpVersion: nil, headerFields: nil)
                            var receivedToken: String?
                            let mockCompletion: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                switch result {
                                case .success(let accountResult):
                                    receivedToken = accountResult["token"] as? String
                                default:
                                    break
                                }
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            expect(receivedToken).toEventually(equal("1234"), timeout: 1)
                        }
                        
                        it("completion handler is run with successful token 12345") {
                            let responseJSON = JSON.dictionary([
                                "account": "http://localhost:8000/api/accounts/99",
                                "auth_token": "12345"
                                ])
                            
                            mockClient.nextResponseData = try! responseJSON.serialize()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 201, httpVersion: nil, headerFields: nil)
                            var receivedToken: String?
                            let mockCompletion: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                switch result {
                                case .success(let accountResult):
                                    receivedToken = accountResult["token"] as? String
                                default:
                                    break
                                }
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            expect(receivedToken).toEventually(equal("12345"), timeout: 1)
                        }
                    }
                    
                    context("failure") {
                        it("completion handler is run with network availability error") {
                            mockClient.nextError = NSError(domain: "test", code: 1, userInfo: nil)
                            var receivedError = false
                            let mockCompletionHandler: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: ResponseError.networkUnavailable)
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletionHandler)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with successful result, but bad data") {
                            mockClient.nextResponseData = Data()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: ResponseError.couldNotParseJSON)
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
   
                        it("completion handler is run with 400") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)
                            var receivedError = false
                            let mockCompletion:(_ result: ResultStringAny) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: ResponseError.unknown)
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with 422 bad request") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 422, httpVersion: nil, headerFields: nil)
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                switch result {
                                case .failure(let error):
                                    switch(error) {
                                    case .badRequest:
                                        receivedError = true
                                    default:
                                        break
                                    }
                                default:
                                    break
                                }
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with a 422") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 422, httpVersion: nil, headerFields: nil)
                            let errors = JSON.array(["some error message"])
                            mockClient.nextResponseData = try! JSON.dictionary(["errors": errors]).serialize()
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringAny) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: ResponseError.badRequest("some error message"))
                            }
                            
                            subject.createAccount(email: "test@example.com", password: "password", confirmPassword: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                    }
                }
                
                describe("createSession") {
                    context("success") {
                        it("has matching URL") {
                            subject.createSession(email: "test@example.com", password: "password", completion: { (result) in })
                            expect(mockClient.sentURL).to(equal("http://localhost:8000/api/sessions"))
                        }
                        
                        it("completion handler is run with successful result") {
                            mockClient.nextResponseData = try! JSON.dictionary([
                                "token": .string("1234")]).serialize()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                            var receivedResult: [String: NSObject] = ["token": "" as NSObject]
                            let mockCompletion: (_ result: ResultStringObject) -> Void = {
                                (result) in
                                switch result {
                                case .success(let authToken):
                                    receivedResult["token"] = authToken["token"]
                                default:
                                    break
                                }
                            }
                            
                            subject.createSession(email: "test@example.com", password: "password", completion: mockCompletion)
                            expect(receivedResult["token"] as? String).toEventually(equal("1234"), timeout: 1)
                        }
                    }
                    
                    context("failure") {
                        it("completion handler is run with 422 bad request") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 422, httpVersion: nil, headerFields: nil)
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringObject) -> Void = {
                                (result) in
                                switch result {
                                case .failure(let error):
                                    switch error {
                                    case .badRequest:
                                        receivedError = true
                                    default:
                                        break
                                    }
                                default:
                                    break
                                }
                            }
                            
                            subject.createSession(email: "test@example.com", password: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with network availability errors") {
                            mockClient.nextError = NSError(domain: "test", code: 1, userInfo: nil)
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringObject) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .networkUnavailable)
                            }
                            
                            subject.createSession(email: "test@example.com", password: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with successful result, but bad data") {
                            mockClient.nextResponseData = Data()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringObject) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .couldNotParseJSON)
                            }
                            
                            subject.createSession(email: "test@example.com", password: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with a 400") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringObject) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .unknown)
                            }
                            
                            subject.createSession(email: "test@example.com", password: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with a 422") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 422, httpVersion: nil, headerFields: nil)
                            let errors = JSON.array(["Some error message"])
                            mockClient.nextResponseData = try! JSON.dictionary(["errors": errors]).serialize()
                            var receivedError = false
                            let mockCompletion: (_ result: ResultStringObject) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .badRequest("Some error message"))
                            }
                            subject.createSession(email: "test@example.com", password: "password", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                    }
                    
                }
            }
            
            describe("READ") {
                describe("readTicker for bitcoin") {
                    context("success") {
                        it("has matching URL") {
                            subject.readTickerFor(coin: "bitcoin", completion: { (result) in })
                            
                            expect(mockClient.sentURL).to(equal("http://localhost:8000/api/ticker/bitcoin"))
                        }
                        
                        it("completion handler is run with successful result") {
                            let mainContext = mockCoreDataStack.mainContext
                            let mockCoin = Coin(context: mainContext, name: "Bitcoin")
                            mockClient.nextResponseData = try! [mockCoin].toJSON().serialize()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)

                            var receivedCoin = Coin(context: mainContext)
                            let mockCompletion: (_ result: ResultJSON) -> Void = {
                                (result) in
                                switch result {
                                case .success(let coinJson):
                                    receivedCoin = try! Coin(context: mainContext, json: coinJson)
                                default:
                                    break

                                }
                            }

                            subject.readTickerFor(coin: "bitcoin", completion: mockCompletion)
                            expect(receivedCoin.toJSON()).toEventually(equal(mockCoin.toJSON()), timeout: 1)
                        }
                    }
                    
                    context("failure") {
                        it("completion handler is run with failure result") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)
                            
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSON) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .unknown)
                            }
                            
                            subject.readTickerFor(coin: "bitcoin", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with network unavailable error") {
                            mockClient.nextError = NSError(domain: "test", code: 1, userInfo: nil)
                            
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSON) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .networkUnavailable)
                            }
                            
                            subject.readTickerFor(coin: "bitcoin", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with successful result, but bad data") {
                            mockClient.nextResponseData = Data()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                            
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSON) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .couldNotParseJSON)
                            }
                            
                            subject.readTickerFor(coin: "bitcoin", completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion hander is run with 422") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 422, httpVersion: nil, headerFields: nil)
                            
                            let error = JSON.string("id not found")
                            mockClient.nextResponseData = try! JSON.dictionary(["error": error]).serialize()
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSON) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .badRequest("id not found"))
                            }
                            
                            subject.readTickerFor(coin: "willIsReallyCoolCoin", completion: mockCompletion)
                            
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                    }
                }
                
                describe("readSupportedCoins") {
                    context("success") {
                        it("has matching URL") {
                            subject.readSupportedCoins(completion: { (result) in })
                            
                            expect(mockClient.sentURL).to(equal("http://localhost:8000/api/supportedCoins"))
                        }
                        
                        it("completion handler is run with a successful result") {
                            let mainContext = mockCoreDataStack.mainContext
                            
                            let mockCoin1 = Coin(context: mainContext, name: "Bitcoin")
                            let mockCoin2 = Coin(context: mainContext, name: "Ethereum")
                            
                            let mockJsonArray = [mockCoin1, mockCoin2].toJSON()
                            mockClient.nextResponseData = try! mockJsonArray.serialize()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                            
                            var receivedResult = [JSON]()
                            let mockCompletion: (_ result: ResultJSONArray) -> Void = {
                                (result) in
                                switch result {
                                case .success(let coin):
                                    receivedResult = coin
                                default:
                                    break
                                }
                            }
                            
                            subject.readSupportedCoins(completion: mockCompletion)
                            expect(receivedResult).toEventually(equal([mockCoin1.toJSON(), mockCoin2.toJSON()]), timeout: 1)
                        }
                    }
                    
                    context("failure") {
                        it("completion handler is run with a failure result") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)
                            
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSONArray) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .unknown)
                            }
                            
                            subject.readSupportedCoins(completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with network unavailable error") {
                            mockClient.nextError = NSError(domain: "test", code: 1, userInfo: nil)
                            
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSONArray) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .networkUnavailable)
                            }
                            
                            subject.readSupportedCoins(completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion handler is run with successful result, but bad data") {
                            mockClient.nextResponseData = Data()
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                            
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSONArray) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .couldNotParseJSON)
                            }
                            
                            subject.readSupportedCoins(completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                        
                        it("completion hander is run with 422") {
                            mockClient.nextResponse = HTTPURLResponse(url: url, statusCode: 422, httpVersion: nil, headerFields: nil)
                            
                            let error = JSON.string("error")
                            mockClient.nextResponseData = try! JSON.dictionary(["error" : error]).serialize()
                            var receivedError = false
                            let mockCompletion: (_ result: ResultJSONArray) -> Void = {
                                (result) in
                                receivedError = self.resultHasError(result, responseError: .badRequest("error"))
                            }
                            
                            subject.readSupportedCoins(completion: mockCompletion)
                            expect(receivedError).toEventually(beTrue(), timeout: 1)
                        }
                    }
                }
            }
            
            describe("UPDATE") {
            }
            
            describe("DELETE") {
                describe("deleteSession") {
                    
                }
            }
            
            describe("parseError") {
                
            }
        }
    }
}
