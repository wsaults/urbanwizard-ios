//
//  HTTPClientSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import Freddy
@testable import UrbanWizard

class HTTPClientSpec: QuickSpec {
    override func spec() {
        var subject: HTTPClient!
        var mockUrlSession: MockURLSession!
        var mockSession: MockSession!
        var url: URL!
        var body: JSON!
        
        describe("HTTPClient") {
            beforeEach {
                mockUrlSession = MockURLSession()
                subject = HTTPClient()
                subject.urlSession = mockUrlSession
                mockSession = MockSession()
                subject.session = mockSession
                
                url = URL(string: "http://appventur.es")
                body = JSON.dictionary(["key": "value".toJSON()])
            }
            
            describe("GET") {
                it("request has GET HTTPMethod") {
                    let expectedMthod = "GET"
                    
                    subject.get(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPMethod).to(equal(expectedMthod))
                }
                
                it("get should change the lat url") {
                    subject.get(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastURL).to(equal(url))
                }
                
                it("data task should call resume") {
                    let dataTask = MockURLSessionDataTask()
                    mockUrlSession.nextDataTask = dataTask
                    
                    subject.get(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(dataTask.resumeWasCalled).to(beTrue())
                }
                
                it("request has headers with the expected application/jason and x-auth-token") {
                    mockSession.authToken = "whatever"
                    let expectedHeaders = ["Content-Type": "application/json", "x-auth-token": "whatever"]
                    
                    subject.get(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPHeaders).to(equal(expectedHeaders))
                }
                
                it("gets the response we expect") {
                    let expectedResponse = HTTPURLResponse(url: URL(string: "http://appventur.es")!, statusCode: 200, httpVersion: nil, headerFields: nil)
                    mockUrlSession.nextResponse = expectedResponse
                    
                    var actualResponse: URLResponse?
                    subject.get(url: url, completion: {
                        (_, response, _) in
                        actualResponse = response
                    })
                    
                    expect(actualResponse) === expectedResponse
                }
            }
            
            describe("DELETE") {
                it("request has DELETE HTTPMethod") {
                    let expectedMethod = "DELETE"
                    
                    subject.delete(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPMethod).to(equal(expectedMethod))
                }
                
                it("delete should change the last url") {
                    subject.delete(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastURL).to(equal(url))
                }
                
                it("data task should call resume") {
                    let dataTask = MockURLSessionDataTask()
                    mockUrlSession.nextDataTask = dataTask
                    
                    subject.delete(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(dataTask.resumeWasCalled).to(beTrue())
                }
                
                it("request has headers with the expected application/json and x-auth-token") {
                    mockSession.authToken = "whatever"
                    let expectedHeaders = ["Content-Type": "application/json", "x-auth-token": "whatever"]
                    
                    subject.delete(url: url) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPHeaders).to(equal(expectedHeaders))
                }
                
                it("deletes the response we exptect") {
                    let expectedResponse = HTTPURLResponse(url: URL(string: "http://appventur.es")!, statusCode: 200, httpVersion: nil, headerFields: nil)
                    mockUrlSession.nextResponse = expectedResponse
                    
                    var actualResponse: URLResponse?
                    subject.delete(url: url, completion: {
                        (_, response, _) in
                        actualResponse = response
                    })
                    
                    expect(actualResponse) === expectedResponse
                }
            }
            
            describe("POST") {
                it("request has POST HTTPMethod") {
                    let expectedMethod = "POST"
                    
                    subject.post(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPMethod).to(equal(expectedMethod))
                }
                
                it("post should change the last url") {
                    subject.post(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastURL).to(equal(url))
                }
                
                it("data task should call resume") {
                    let dataTask = MockURLSessionDataTask()
                    mockUrlSession.nextDataTask = dataTask
                    
                    subject.post(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(dataTask.resumeWasCalled).to(beTrue())
                }
                
                it("request has headers with the expected application/json and x-auth-token") {
                    mockSession.authToken = "whatever"
                    let expectedHeaders = ["Content-Type": "application/json", "x-auth-token": "whatever"]
                    
                    subject.post(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPHeaders).to(equal(expectedHeaders))
                }
                
                it("posts the response we expect") {
                    let expectedResponse = HTTPURLResponse(url: URL(string: "http://appventur.es")!, statusCode: 200, httpVersion: nil, headerFields: nil)
                    mockUrlSession.nextResponse = expectedResponse
                    
                    var actualResponse: URLResponse?
                    subject.post(url: url, body: body, completion: {
                        (_, response, _) in
                        actualResponse = response
                    })
                    
                    expect(actualResponse) === expectedResponse
                }
                
                it("posts the data we expect") {
                    let expectedData = "{}".data(using: String.Encoding.utf8)
                    mockUrlSession.nextData = expectedData
                    
                    var actualData: Data?
                    subject.post(url: url, body: body, completion: {
                        (data, _, _) in
                        actualData = data
                    })
                    
                    expect(actualData) === expectedData
                }
            }
            
            describe("PUT") {
                it("has the PUT HTTPMethod") {
                    let expectedMethod = "PUT"
                    
                    subject.put(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPMethod).to(equal(expectedMethod))
                }
                
                it("should change the lat url") {
                    subject.put(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastURL).to(equal(url))
                }
                
                it("data task should call resume") {
                    let dataTask = MockURLSessionDataTask()
                    mockUrlSession.nextDataTask = dataTask
                    
                    subject.put(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(dataTask.resumeWasCalled).to(beTrue())
                }
                
                it("request has headers with the expected application/json and x-auth-token") {
                    mockSession.authToken = "whatever"
                    let exptectedHeaders = ["Content-Type": "application/json", "x-auth-token": "whatever"]
                    
                    subject.put(url: url, body: body) {
                        (_, _, _) -> Void in }
                    
                    expect(mockUrlSession.lastHTTPHeaders).to(equal(exptectedHeaders))
                }
                
                it("puts the response we expect") {
                    let expectedResponse = HTTPURLResponse(url: URL(string: "http://appventur.es")!, statusCode: 200, httpVersion: nil, headerFields: nil)
                    mockUrlSession.nextResponse = expectedResponse
                    
                    var actualResponse: URLResponse?
                    subject.put(url: url, body: body, completion: {
                        (_, resonse, _) in
                        actualResponse = resonse
                    })
                    
                    expect(actualResponse) === expectedResponse
                }
                
                it("puts the data we expect") {
                    let expectedData = "{}".data(using: String.Encoding.utf8)
                    mockUrlSession.nextData = expectedData
                    
                    var actualData: Data?
                    subject.put(url: url, body: body, completion: {
                        (data, _, _) in
                        actualData = data
                    })
                    
                    expect(actualData) === expectedData
                }
            }
        }
    }
}

class MockURLSession: URLSessionProtocol {
    var nextData: Data?
    var nextResponse: URLResponse?
    var nextError: Error?
    
    var nextDataTask = MockURLSessionDataTask()
    fileprivate (set) var lastURL: URL?
    fileprivate (set) var lastHTTPHeaders: [String:String]?
    fileprivate (set) var lastHTTPMethod: String?
    
    func dataTaskWithRequest(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        lastURL = request.url
        lastHTTPHeaders = request.allHTTPHeaderFields
        lastHTTPMethod = request.httpMethod
        completionHandler(nextData, nextResponse, nextError)
        return nextDataTask
    }
}

class MockURLSessionDataTask: URLSessionDataTaskProtocol {
    fileprivate (set) var resumeWasCalled = false
    
    func resume() {
        resumeWasCalled = true
    }
}
