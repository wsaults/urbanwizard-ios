//
//  RequirementLabelViewSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/9/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class RequirementLabelViewSpec: QuickSpec {
    override func spec() {
        describe("RequirementLabelView") {
            var subject: RequirementLabelView!
            let mockLabel = UILabel()
            let mockDotView = UIView()
            
            beforeEach {
                subject = RequirementLabelView()
                
                mockLabel.text = ""
                subject.label = mockLabel
                subject.label.textColor = UIColor.black
                
                mockDotView.backgroundColor = UIColor.black
                subject.dotView = mockDotView
            }
            
            describe("requirment isMet") {
                context("enabled is true") {
                    it("label color should match when met") {
                        subject.requirement(isMet: true)
                        
                        expect(subject.label.textColor).to(equal(UIColor.green))
                    }
                    
                    it("dotView color should match when met") {
                        subject.requirement(isMet: true)
                        
                        expect(subject.dotView.backgroundColor).to(equal(UIColor.green))
                    }
                }
                
                context("enabled is false") {
                    it("label color should match when not met") {
                        subject.requirement(isMet: false)
                        
                        expect(subject.label.textColor).to(equal(UIColor.black))
                    }
                    
                    it("dotView color should match when not met") {
                        subject.requirement(isMet: false)
                        
                        expect(subject.dotView.backgroundColor).to(equal(UIColor.black))
                    }
                }
            }
        }
    }
}
