//
//  HomeViewModelSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/10/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class HomeViewModelSpec: QuickSpec {
    override func spec() {
        describe("HomeViewModel") {
            var subject: HomeViewModel!
            var mockTableView: UITableView!
            var mockCoreDataStack: MockCoreDataStack!
            
            beforeEach {
                subject = HomeViewModel()
                mockCoreDataStack = MockCoreDataStack()
                subject.coreDataStack = mockCoreDataStack
                
                mockTableView = UITableView()
                subject.tableView = mockTableView
            }
        }
    }
}
