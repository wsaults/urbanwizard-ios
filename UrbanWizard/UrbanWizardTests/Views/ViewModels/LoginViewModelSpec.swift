//
//  LoginViewModelSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/11/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class LoginViewModelSpec: QuickSpec {
    override func spec() {
        describe("LoginViewModel") {
            var subject: LoginViewModel!
            
            var mockEmailView: TextFieldAndLabelView!
            var mockEmailTextField: UITextField!
            var mockEmailValidIndicator: UIView!
            var mockEmailConstraint: NSLayoutConstraint!
            
            var mockPasswordView: TextFieldAndLabelView!
            var mockPasswordTextField: UITextField!
            var mockPasswordValidIndicator: UIView!
            var mockPasswordConstraint: NSLayoutConstraint!
            
            beforeEach {
                subject = LoginViewModel()
                
                mockEmailView = TextFieldAndLabelView()
                mockEmailTextField = UITextField()
                mockEmailView.textField = mockEmailTextField
                mockEmailValidIndicator = UIView()
                mockEmailView.validIndicator = mockEmailValidIndicator
                mockEmailConstraint = NSLayoutConstraint()
                mockEmailConstraint.constant = 100.0
                mockEmailView.widthConstraint = mockEmailConstraint
                subject.emailView = mockEmailView
                
                mockPasswordView = TextFieldAndLabelView()
                mockPasswordTextField = UITextField()
                mockPasswordView.textField = mockPasswordTextField
                mockPasswordValidIndicator = UIView()
                mockPasswordView.validIndicator = mockPasswordValidIndicator
                mockPasswordConstraint = NSLayoutConstraint()
                mockPasswordConstraint.constant = 100.0
                mockPasswordView.widthConstraint = mockPasswordConstraint
                subject.passwordView = mockPasswordView
            }
            
            describe("emailIsValid") {
                beforeEach {
                    subject.emailView.textField.text = ""
                    subject.emailView.awakeFromNib()
                }
                
                context("returns true") {
                    it("when email matches test@example.com") {
                        subject.emailView.textField.text = "test@example.com"
                        expect(subject.emailIsValid()).to(beTrue())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.emailView.textField.text = "test@example.com"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("emailView widthConstraint matches 100%") {
                        subject.emailView.textField.text = "test@example.com"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.widthConstraint.constant).to(equal(100.0))
                    }
                }
                
                context("returns false") {
                    it("when email is not valid") {
                        subject.emailView.textField.text = "lameemail"
                        expect(subject.emailIsValid()).to(beFalse())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.emailView.textField.text = "lameemail"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("emailView widthConstraint matches 100%") {
                        subject.emailView.textField.text = "lameemail"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.widthConstraint.constant).to(equal(100.0))
                    }
                }
            }
            
            describe("passwordIsValid") {
                beforeEach {
                    subject.passwordView.textField.text = ""
                    subject.passwordView.awakeFromNib()
                }
                
                context("returns true") {
                    it("password is more than 7 characters") {
                        subject.passwordView.textField.text = "Password1!"
                        expect(subject.passwordIsValid()).to(beTrue())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.passwordView.textField.text = "Password1!"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                }
                
                context("returns false") {
                    it("password is less than 8 characters") {
                        subject.passwordView.textField.text = "lamepas"
                        expect(subject.passwordIsValid()).to(beFalse())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.passwordView.textField.text = "lamepas"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                }
            }
        }
    }
}
