//
//  PinViewModelSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/22/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class PinViewModelSpec: QuickSpec {
    override func spec() {
        describe("PinViewModel") {
            var subject: PinViewModel!
            var mockPinEntryLabel: UILabel!
            var mockOneButton: UIButton!
            var mockTwoButton: UIButton!
            var mockThreeButton: UIButton!
            var mockFourButton: UIButton!
            var mockFiveButton: UIButton!
            var mockSixButton: UIButton!
            var mockSevenButton: UIButton!
            var mockEightButton: UIButton!
            var mockNineButton: UIButton!
            var mockZeroButton: UIButton!
            
            beforeEach {
                subject = PinViewModel()
                
                mockPinEntryLabel = UILabel()
                subject.pinEntryLabel = mockPinEntryLabel
                
                mockOneButton = UIButton()
                mockOneButton.titleLabel?.text = "1"
                subject.oneButton = mockOneButton
                
                mockTwoButton = UIButton()
                mockTwoButton.titleLabel?.text = "2"
                subject.twoButton = mockTwoButton
                
                mockThreeButton = UIButton()
                mockThreeButton.titleLabel?.text = "3"
                subject.threeButton = mockThreeButton
                
                mockFourButton = UIButton()
                mockFourButton.titleLabel?.text = "4"
                subject.fourButton = mockFourButton
                
                mockFiveButton = UIButton()
                mockFiveButton.titleLabel?.text = "5"
                subject.fiveButton = mockFiveButton
                
                mockSixButton = UIButton()
                mockSixButton.titleLabel?.text = "6"
                subject.sixButton = mockSixButton
                
                mockSevenButton = UIButton()
                mockSevenButton.titleLabel?.text = "7"
                subject.sevenButton = mockSevenButton
                
                mockEightButton = UIButton()
                mockEightButton.titleLabel?.text = "8"
                subject.eightButton = mockEightButton
                
                mockNineButton = UIButton()
                mockNineButton.titleLabel?.text = "9"
                subject.nineButton = mockNineButton
                
                mockZeroButton = UIButton()
                mockZeroButton.titleLabel?.text = "0"
                subject.zeroButton = mockZeroButton
            }
            
            describe("pinIsValid") {
                beforeEach {
                    subject.pinCode = ""
                }
                
                it("should return true when pinCode is four characters long") {
                    subject.pinCode = "1234"
                    
                    expect(subject.pinIsValid()).to(beTrue())
                }
                
                it("should return false when pinCode is less than four characters long") {
                    subject.pinCode = "123"
                    
                    expect(subject.pinIsValid()).to(beFalse())
                }
                
                it("should return false when pinCode contains anything besides numbers") {
                    subject.pinCode = "1ab4"
                    
                    expect(subject.pinIsValid()).to(beFalse())
                }
            }
            
            describe("pinKeyPressed") {
                beforeEach {
                    subject.pinCode = ""
                }
                
                context("pincode") {
                    it("should match 1") {
                        subject.pinKeyPressed(subject.oneButton)
                        
                        expect(subject.pinCode).to(equal("1"))
                    }
                    
                    it("should match 2") {
                        subject.pinKeyPressed(subject.twoButton)
                        
                        expect(subject.pinCode).to(equal("2"))
                    }
                    
                    it("should match 23") {
                        subject.pinKeyPressed(subject.twoButton)
                        subject.pinKeyPressed(subject.threeButton)
                        
                        expect(subject.pinCode).to(equal("23"))
                    }
                    
                    it("should match 345") {
                        subject.pinKeyPressed(subject.threeButton)
                        subject.pinKeyPressed(subject.fourButton)
                        subject.pinKeyPressed(subject.fiveButton)
                        
                        expect(subject.pinCode).to(equal("345"))
                    }
                    
                    it("should match 6789") {
                        subject.pinKeyPressed(subject.sixButton)
                        subject.pinKeyPressed(subject.sevenButton)
                        subject.pinKeyPressed(subject.eightButton)
                        subject.pinKeyPressed(subject.nineButton)
                        
                        expect(subject.pinCode).to(equal("6789"))
                    }
                    
                    it("should match 0531") {
                        subject.pinKeyPressed(subject.zeroButton)
                        subject.pinKeyPressed(subject.fiveButton)
                        subject.pinKeyPressed(subject.threeButton)
                        subject.pinKeyPressed(subject.oneButton)
                        
                        expect(subject.pinCode).to(equal("0531"))
                    }
                    
                    it("should match 6789 when 67890 are pressed") {
                        subject.pinKeyPressed(subject.sixButton)
                        subject.pinKeyPressed(subject.sevenButton)
                        subject.pinKeyPressed(subject.eightButton)
                        subject.pinKeyPressed(subject.nineButton)
                        subject.pinKeyPressed(subject.zeroButton)
                        
                        expect(subject.pinCode).to(equal("6789"))
                    }
                }
                
                context("pinEntryLabel") {
                    it("should match ●") {
                        subject.pinKeyPressed(subject.oneButton)
                        
                        expect(subject.pinEntryLabel.text).to(equal("●"))
                    }
                    
                    it("should match ●●") {
                        subject.pinKeyPressed(subject.twoButton)
                        subject.pinKeyPressed(subject.threeButton)
                        
                        expect(subject.pinEntryLabel.text).to(equal("●●"))
                    }
                    
                    it("should match ●●●") {
                        subject.pinKeyPressed(subject.threeButton)
                        subject.pinKeyPressed(subject.fourButton)
                        subject.pinKeyPressed(subject.fiveButton)
                        
                        expect(subject.pinEntryLabel.text).to(equal("●●●"))
                    }
                    
                    it("should match ●●●●") {
                        subject.pinKeyPressed(subject.sixButton)
                        subject.pinKeyPressed(subject.sevenButton)
                        subject.pinKeyPressed(subject.eightButton)
                        subject.pinKeyPressed(subject.nineButton)
                        
                        expect(subject.pinEntryLabel.text).to(equal("●●●●"))
                    }
                    
                    it("should match ●●●● five buttons are pressed") {
                        subject.pinKeyPressed(subject.sixButton)
                        subject.pinKeyPressed(subject.sevenButton)
                        subject.pinKeyPressed(subject.eightButton)
                        subject.pinKeyPressed(subject.nineButton)
                        subject.pinKeyPressed(subject.zeroButton)
                        
                        expect(subject.pinEntryLabel.text).to(equal("●●●●"))
                    }
                    
                    it("should match pincode length of 1") {
                        subject.pinKeyPressed(subject.oneButton)
                        
                        expect(subject.pinEntryLabel.text?.count).to(equal(1))
                    }
                    
                    it("should match pincode length of 2") {
                        subject.pinKeyPressed(subject.twoButton)
                        subject.pinKeyPressed(subject.threeButton)
                        
                        expect(subject.pinEntryLabel.text?.count).to(equal(2))
                    }
                    
                    it("should match pincode length of 3") {
                        subject.pinKeyPressed(subject.threeButton)
                        subject.pinKeyPressed(subject.fourButton)
                        subject.pinKeyPressed(subject.fiveButton)
                        
                        expect(subject.pinEntryLabel.text?.count).to(equal(3))
                    }
                    
                    it("should match pincode length of 4") {
                        subject.pinKeyPressed(subject.sixButton)
                        subject.pinKeyPressed(subject.sevenButton)
                        subject.pinKeyPressed(subject.eightButton)
                        subject.pinKeyPressed(subject.nineButton)
                        
                        expect(subject.pinEntryLabel.text?.count).to(equal(4))
                    }
                    
                    it("should match pincode length of 4 when five buttons are pressed") {
                        subject.pinKeyPressed(subject.sixButton)
                        subject.pinKeyPressed(subject.sevenButton)
                        subject.pinKeyPressed(subject.eightButton)
                        subject.pinKeyPressed(subject.nineButton)
                        subject.pinKeyPressed(subject.zeroButton)
                        
                        expect(subject.pinEntryLabel.text?.count).to(equal(4))
                    }
                }
            }
        }
    }
}
