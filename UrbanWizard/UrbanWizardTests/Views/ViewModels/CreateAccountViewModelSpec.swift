//
//  CreateAccountViewModelSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/9/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class CreateAccountViewModelSpec: QuickSpec {
    override func spec() {
        describe("CreateAccountViewModel") {
            var subject: CreateAccountViewModel!
            
            var mockEmailView: TextFieldAndLabelView!
            var mockEmailTextField: UITextField!
            var mockEmailValidIndicator: UIView!
            var mockEmailConstraint: NSLayoutConstraint!
            
            var mockPasswordView: TextFieldAndLabelView!
            var mockPasswordTextField: UITextField!
            var mockPasswordValidIndicator: UIView!
            var mockPasswordConstraint: NSLayoutConstraint!
            
            var mockConfirmPasswordView: TextFieldAndLabelView!
            var mockConfirmPasswordTextField: UITextField!
            var mockConfirmPasswordValidIndicator: UIView!
            var mockConfirmPasswordConstraint: NSLayoutConstraint!
            
            var mockMinEightCharsLabel: UILabel!
            var mockMinEightCharsDotView: UIView!
            var mockMinEightCharsView: RequirementLabelView!
            
            var mockMinOneNumberLabel: UILabel!
            var mockMinOneNumberDotView: UIView!
            var mockMinOneNumberView: RequirementLabelView!
            
            var mockMinOneUpperCaseLabel: UILabel!
            var mockMinOneUpperCaseDotView: UIView!
            var mockMinOneUpperCaseView: RequirementLabelView!
            
            var mockMinOneLowerCaseLabel: UILabel!
            var mockMinOneLowerCaseDotView: UIView!
            var mockMinOneLowerCaseView: RequirementLabelView!
            
            var mockMinOneSpecialCharacterLabel: UILabel!
            var mockMinOneSpecialCharacterDotView: UIView!
            var mockMinOneSpecialCharacterView: RequirementLabelView!
            
            beforeEach {
                subject = CreateAccountViewModel()
                
                mockEmailView = TextFieldAndLabelView()
                mockEmailTextField = UITextField()
                mockEmailView.textField = mockEmailTextField
                mockEmailValidIndicator = UIView()
                mockEmailView.validIndicator = mockEmailValidIndicator
                mockEmailConstraint = NSLayoutConstraint()
                mockEmailConstraint.constant = 100.0
                mockEmailView.widthConstraint = mockEmailConstraint
                subject.emailView = mockEmailView
                
                mockPasswordView = TextFieldAndLabelView()
                mockPasswordTextField = UITextField()
                mockPasswordView.textField = mockPasswordTextField
                mockPasswordValidIndicator = UIView()
                mockPasswordView.validIndicator = mockPasswordValidIndicator
                mockPasswordConstraint = NSLayoutConstraint()
                mockPasswordConstraint.constant = 100.0
                mockPasswordView.widthConstraint = mockPasswordConstraint
                subject.passwordView = mockPasswordView
                
                mockConfirmPasswordView = TextFieldAndLabelView()
                mockConfirmPasswordTextField = UITextField()
                mockConfirmPasswordView.textField = mockConfirmPasswordTextField
                mockConfirmPasswordValidIndicator = UIView()
                mockConfirmPasswordView.validIndicator = mockConfirmPasswordValidIndicator
                mockConfirmPasswordConstraint = NSLayoutConstraint()
                mockConfirmPasswordConstraint.constant = 100.0
                mockConfirmPasswordView.widthConstraint = mockConfirmPasswordConstraint
                subject.confirmPasswordView = mockConfirmPasswordView
                
                mockMinEightCharsView = RequirementLabelView()
                mockMinEightCharsLabel = UILabel()
                mockMinEightCharsView.label = mockMinEightCharsLabel
                mockMinEightCharsView.label.text = "Min Password length"
                mockMinEightCharsDotView = UIView()
                mockMinEightCharsView.dotView = mockMinEightCharsDotView
                mockMinEightCharsView.dotView.backgroundColor = UIColor.black
                subject.minimumEightCharactersView = mockMinEightCharsView
                
                mockMinOneNumberView = RequirementLabelView()
                mockMinOneNumberLabel = UILabel()
                mockMinOneNumberView.label = mockMinOneNumberLabel
                mockMinOneNumberView.label.text = "Min One Number"
                mockMinOneNumberDotView = UIView()
                mockMinOneNumberView.dotView = mockMinOneNumberDotView
                mockMinOneNumberView.dotView.backgroundColor = UIColor.black
                subject.minimumOneNumberView = mockMinOneNumberView
                
                mockMinOneUpperCaseView = RequirementLabelView()
                mockMinOneUpperCaseLabel = UILabel()
                mockMinOneUpperCaseView.label = mockMinOneUpperCaseLabel
                mockMinOneUpperCaseView.label.text = "Min One Uppercase"
                mockMinOneUpperCaseDotView = UIView()
                mockMinOneUpperCaseView.dotView = mockMinOneUpperCaseDotView
                mockMinOneUpperCaseView.dotView.backgroundColor = UIColor.black
                subject.minimumOneUpperCaseView = mockMinOneUpperCaseView
                
                mockMinOneLowerCaseView = RequirementLabelView()
                mockMinOneLowerCaseLabel = UILabel()
                mockMinOneLowerCaseView.label = mockMinOneLowerCaseLabel
                mockMinOneLowerCaseView.label.text = "Min One Lowercase"
                mockMinOneLowerCaseDotView = UIView()
                mockMinOneLowerCaseView.dotView = mockMinOneLowerCaseDotView
                mockMinOneLowerCaseView.dotView.backgroundColor = UIColor.black
                subject.minimumOneLowerCaseView = mockMinOneLowerCaseView
                
                mockMinOneSpecialCharacterView = RequirementLabelView()
                mockMinOneSpecialCharacterLabel = UILabel()
                mockMinOneSpecialCharacterView.label = mockMinOneSpecialCharacterLabel
                mockMinOneSpecialCharacterView.label.text = "Min One Special Char"
                mockMinOneSpecialCharacterDotView = UIView()
                mockMinOneSpecialCharacterView.dotView = mockMinOneSpecialCharacterDotView
                mockMinOneSpecialCharacterView.dotView.backgroundColor = UIColor.black
                subject.minimumOneSpecialCharacterView = mockMinOneSpecialCharacterView
            }
            
            describe("emailIsValid") {
                beforeEach {
                    subject.emailView.textField.text = ""
                    subject.emailView.awakeFromNib()
                }
                
                context("returns true") {
                    it("when email matches test@example.com") {
                        subject.emailView.textField.text = "test@example.com"
                        expect(subject.emailIsValid()).to(beTrue())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.emailView.textField.text = "test@example.com"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("emailView widthConstraint matches 100%") {
                        subject.emailView.textField.text = "test@example.com"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.widthConstraint.constant).to(equal(100.0))
                    }
                }
                
                context("returns false") {
                    it("when email is not valid") {
                        subject.emailView.textField.text = "lameemail"
                        expect(subject.emailIsValid()).to(beFalse())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.emailView.textField.text = "lameemail"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("emailView widthConstraint matches 100%") {
                        subject.emailView.textField.text = "lameemail"
                        
                        let _ = subject.emailIsValid()
                        
                        expect(subject.emailView.widthConstraint.constant).to(equal(100.0))
                    }
                }
            }
            
            describe("passwordIsValid") {
                beforeEach {
                    subject.passwordView.textField.text = ""
                    subject.passwordView.awakeFromNib()
                }
                
                context("returns true") {
                    it("password matches Password1!") {
                        subject.passwordView.textField.text = "Password1!"
                        expect(subject.passwordIsValid()).to(beTrue())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.passwordView.textField.text = "Password1!"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                }
                
                context("returns false") {
                    it("password is invalid") {
                        subject.passwordView.textField.text = "lamepassword"
                        expect(subject.passwordIsValid()).to(beFalse())
                    }
                    
                    it("validIndicator background color matches red when the password is more than 64 characters") {
                        subject.passwordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v1"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("validIndicator background color matches red when the password is invalid") {
                        subject.passwordView.textField.text = "invalid ` password"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("password widthConstraint matches 100 when the password is more than 64 characters") {
                        subject.passwordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v1"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(100.0))
                    }
                    
                    it("password widthConstraint matches 100 when the password is invalid") {
                        subject.passwordView.textField.text = "b a d p a s s"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(100.0))
                    }
                }
                
                describe("validPercent constraint") {
                    it("matches 0% when no conditions are met") {
                        subject.passwordView.textField.text = ""
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(0.0))
                    }
                    
                    it("matches 20% when one condition is met") {
                        subject.passwordView.textField.text = "p"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(20.0))
                    }
                    
                    it("matches 40% when two conditions are met") {
                        subject.passwordView.textField.text = "p1"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(40.0))
                    }
                    
                    it("matches 60% when three conditions are met") {
                        subject.passwordView.textField.text = "p1A"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(beCloseTo(60.0, within: 0.01)) // Some strange precision going on here...
                    }
                    
                    it("matches 80% when four conditions are met") {
                        subject.passwordView.textField.text = "p1A!"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(80.0))
                    }
                    
                    it("matches 100% when five conditions are met") {
                        subject.passwordView.textField.text = "p1A!word"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.passwordView.widthConstraint.constant).to(equal(100.0))
                    }
                }
                
                describe("minimum eight characters") {
                    it("text color should match when password contains at least eight characters") {
                        subject.passwordView.textField.text = "password"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumEightCharactersView.label.textColor).to(equal(UIColor.green))
                    }
                    
                    it("dot background color should match when password contains at least eight characters") {
                        subject.passwordView.textField.text = "lamepassword"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumEightCharactersView.dotView.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("text color should match when password contains less than eight characters") {
                        subject.minimumEightCharactersView.requirement(isMet: true)
                        subject.passwordView.textField.text = "pass"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumEightCharactersView.label.textColor).to(equal(UIColor.black))
                    }
                    
                    it("dot background color should match when password contains less than eight characters") {
                        subject.minimumEightCharactersView.requirement(isMet: true)
                        subject.passwordView.textField.text = ""
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumEightCharactersView.dotView.backgroundColor).to(equal(UIColor.black))
                    }
                }
                
                describe("minimum one number") {
                    it("text color should match when password contains at least one number") {
                        subject.passwordView.textField.text = "pass1"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneNumberView.label.textColor).to(equal(UIColor.green))
                    }
                    
                    it("dot background color should match when password contains at least one number") {
                        subject.passwordView.textField.text = "1"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneNumberView.dotView.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("text color should match when password does not contain a number") {
                        subject.minimumOneNumberView.requirement(isMet: true)
                        subject.passwordView.textField.text = "pass"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneNumberView.label.textColor).to(equal(UIColor.black))
                    }
                    
                    it("dot background color should match when password does not contain a number") {
                        subject.minimumOneNumberView.requirement(isMet: true)
                        subject.passwordView.textField.text = ""
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneNumberView.dotView.backgroundColor).to(equal(UIColor.black))
                    }
                }
                
                describe("minimum one uppercase") {
                    it("text color should match when password contains at least one uppercase letter") {
                        subject.passwordView.textField.text = "Pass"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneUpperCaseView.label.textColor).to(equal(UIColor.green))
                    }
                    
                    it("dot background color should match when password contains at least one uppercase letter") {
                        subject.passwordView.textField.text = "P"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneUpperCaseView.dotView.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("text color should match when password does not contain an uppercase letter") {
                        subject.minimumOneUpperCaseView.requirement(isMet: true)
                        subject.passwordView.textField.text = "pass"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneUpperCaseView.label.textColor).to(equal(UIColor.black))
                    }
                    
                    it("dot background color should match when password does not contain an uppercase letter") {
                        subject.minimumOneUpperCaseView.requirement(isMet: true)
                        subject.passwordView.textField.text = ""
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneUpperCaseView.dotView.backgroundColor).to(equal(UIColor.black))
                    }
                }
                
                describe("minimum one lowercase") {
                    it("text color should match when password contains at least one lowercase letter") {
                        subject.passwordView.textField.text = "pass"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneLowerCaseView.label.textColor).to(equal(UIColor.green))
                    }
                    
                    it("dot background color should match when password contains at least one lowercase letter") {
                        subject.passwordView.textField.text = "p"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneLowerCaseView.dotView.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("text color should match when password does not contain at least one lowercase letter") {
                        subject.minimumOneLowerCaseView.requirement(isMet: true)
                        subject.passwordView.textField.text = "PASS"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneLowerCaseView.label.textColor).to(equal(UIColor.black))
                    }
                    
                    it("dot background color should match when password does not contain at least one lowercase letter") {
                        subject.minimumOneLowerCaseView.requirement(isMet: true)
                        subject.passwordView.textField.text = ""
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneLowerCaseView.dotView.backgroundColor).to(equal(UIColor.black))
                    }
                }
                
                describe("minimum one special character") {
                    it("text color should match when password contains at least one special character") {
                        subject.passwordView.textField.text = "p*ss"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneSpecialCharacterView.label.textColor).to(equal(UIColor.green))
                    }
                    
                    it("dot background color should match when password contains at least one special character") {
                        subject.passwordView.textField.text = "&"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneSpecialCharacterView.dotView.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("text color should match when password does not contain a special character") {
                        subject.minimumOneSpecialCharacterView.requirement(isMet: true)
                        subject.passwordView.textField.text = "pass"
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneSpecialCharacterView.label.textColor).to(equal(UIColor.black))
                    }
                    
                    it("dot background color should match when password does not contain a special character") {
                        subject.minimumOneSpecialCharacterView.requirement(isMet: true)
                        subject.passwordView.textField.text = ""
                        
                        let _ = subject.passwordIsValid()
                        
                        expect(subject.minimumOneSpecialCharacterView.dotView.backgroundColor).to(equal(UIColor.black))
                    }
                }
            }
            
            describe("doPasswordsMatch") {
                beforeEach {
                    subject.passwordView.textField.text = ""
                    subject.confirmPasswordView.textField.text = ""
                    subject.confirmPasswordView.awakeFromNib()
                }
                
                context("returns true") {
                    it("returns true when password matches confirm password") {
                        subject.passwordView.textField.text = "Password1!"
                        subject.confirmPasswordView.textField.text = "Password1!"
                        expect(subject.doPasswordsMatch()).to(beTrue())
                    }
                    
                    it("validIndicator background color matches") {
                        subject.passwordView.textField.text = "Password1!"
                        subject.confirmPasswordView.textField.text = "Password1!"
                        
                        let _ = subject.doPasswordsMatch()
                        
                        expect(subject.confirmPasswordView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                }
                
                context("returns false") {
                    it("returns false when the passwords do not match") {
                        subject.passwordView.textField.text = "lamepassword"
                        subject.confirmPasswordView.textField.text = "Password1!"
                        
                        expect(subject.doPasswordsMatch()).to(beFalse())
                    }
                    
                    it("validIndicator background color matches red when the passwords do not match") {
                        subject.passwordView.textField.text = "lamepassword"
                        subject.confirmPasswordView.textField.text = "Password1!"
                        
                        let _ = subject.doPasswordsMatch()
                        
                        expect(subject.confirmPasswordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("validIndicator background color matches red when confirm password text is empty") {
                        let _ = subject.doPasswordsMatch()
                        
                        expect(subject.confirmPasswordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("password widthConstraint matches 100 when the passwords do not match") {
                        subject.passwordView.textField.text = "lamepassword"
                        subject.confirmPasswordView.textField.text = "Password1!"
                        
                        let _ = subject.doPasswordsMatch()
                        
                        expect(subject.confirmPasswordView.widthConstraint.constant).to(equal(100.0))
                    }
                }
            }
        }
    }
}
