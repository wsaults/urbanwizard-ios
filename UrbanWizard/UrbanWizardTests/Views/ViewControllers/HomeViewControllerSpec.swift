//
//  HomeViewControllerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/10/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit
import CoreData
import Freddy

@testable import UrbanWizard

class HomeViewControllerSpec: QuickSpec {
    override func spec() {
        describe("HomeViewController") {
            var subject: HomeViewController!
            var mockAlertPresenter: MockAlertPresenter!
            var mockClientManager: MockHTTPClientManager!
            var mockCoreDataStack: MockCoreDataStack!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                subject = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                mockAlertPresenter = MockAlertPresenter()
                subject.alertPresenter = mockAlertPresenter
                
                mockClientManager = MockHTTPClientManager()
                subject.viewModel.clientManager = mockClientManager
                
                mockCoreDataStack = MockCoreDataStack()
                subject.coreDataStack = mockCoreDataStack
                
                subject.view.layoutSubviews()
            }
            
            describe("viewDidLoad") {
                context("content") {
                }
                
                context("load data") {
//                    describe("readTicker") {
//                        it("read coin matches") {
//                            let dictionary: [String:JSON] = [
//                                "name": .string("Bitcoin")
//                            ]
//                            let jsonDictionary = JSON.dictionary(dictionary)
//                            mockClientManager.nextCoinResult = ResultJSON.success(jsonDictionary)
//
//                            subject.viewDidLoad()
//                            
//                            let mainContext = mockCoreDataStack.mainContext
//                            let expectedCoin = Coin(context: mainContext, name: "Bitcoin")
//                            expect(subject.viewModel.receivedCoin.name).to(equal(expectedCoin.name))
//                        }
//                    }
                }
            }
        }
    }
}
