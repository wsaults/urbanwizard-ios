//
//  LoginViewControllerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 11/28/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit
import CoreData

@testable import UrbanWizard

class LoginViewControllerSpec: QuickSpec {
    override func spec() {
        describe("LoginViewController") {
            var subject: LoginViewController!
            var mockSession: MockSession!
            var mockHTTPClientManager: MockHTTPClientManager!
            var mockCoreDataStack: MockCoreDataStack!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Signup", bundle: nil)
                subject = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                mockSession = MockSession()
                subject.viewModel.session = mockSession
                
                mockCoreDataStack = MockCoreDataStack()
                subject.coreDataStack = mockCoreDataStack
                
                mockHTTPClientManager = MockHTTPClientManager()
                subject.viewModel.clientManager = mockHTTPClientManager
                
                subject.view.layoutSubviews()
            }
            
            describe("viewDidLoad") {
                context("content") {
                    it("email address placeholder should match") {
                        expect(subject.viewModel.emailView.textField.placeholder).to(equal("Email Address"))
                    }
                    
                    it("password placeholder should match") {
                        expect(subject.viewModel.passwordView.textField.placeholder).to(equal("Password"))
                    }
                    
                    it("forgot password button text should match") {
                        expect(subject.viewModel.forgotPasswordButton.titleLabel?.text).to(equal("Forgot Password"))
                    }
                    
                    it("done button text should match") {
                        expect(subject.viewModel.signInButton.titleLabel?.text).to(equal("Login"))
                    }
                }
                
                context("setup") {
                    it("email field should be empty") {
                        expect(subject.viewModel.emailView.textField.text).to(beEmpty())
                    }
                    
                    it("password field should be empty") {
                        expect(subject.viewModel.passwordView.textField.text).to(beEmpty())
                    }
                    
                    it("done button should be disabled") {
                        expect(subject.viewModel.signInButton.isEnabled).to(beFalse())
                    }
                    
                    it("viewModel.coreDataStack should not be nil") {
                        expect(subject.viewModel.coreDataStack).toNot(beNil())
                    }
                }
            }
            
            describe("signInButtonTapped") {
                describe("create session") {
                    beforeEach {
                        mockHTTPClientManager.nextDictionaryResult = ResultStringObject.success(["token": "1234" as NSObject])
                        subject.viewModel.clientManager = mockHTTPClientManager
                        mockSession = MockSession()
                        subject.viewModel.session = mockSession
                    }
                    
                    it("session's authToken should match") {
                        subject.createSessionFromTextFields({})
                        
                        expect(mockSession.authToken!).to(equal("1234"))
                    }
                }
            }
            
            describe("textFieldDidChange") {
                context("everything but email is filled out") {
                    beforeEach {
                        subject.viewModel.emailView.textField.text = ""
                        subject.viewModel.passwordView.textField.text = "Password1!"
                    }
                    
                    it("login button should become enabled when email is filled out") {
                        subject.viewModel.emailView.textField.text = "test@example.com"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.signInButton.isEnabled).to(beTrue())
                    }
                    
                    it("login button should become disabled when email is not filled out") {
                        subject.viewModel.emailView.textField.text = ""
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.signInButton.isEnabled).to(beFalse())
                    }
                }
                
                context("everything but password is filled out") {
                    beforeEach {
                        subject.viewModel.emailView.textField.text = "test@example.com"
                        subject.viewModel.passwordView.textField.text = ""
                    }
                    
                    it("login button should become enabled when password is filled out") {
                        subject.viewModel.passwordView.textField.text = "Password1!"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.signInButton.isEnabled).to(beTrue())
                    }
                    
                    it("login butotn should become disabled when password is not filled out") {
                        subject.viewModel.passwordView.textField.text = ""
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.signInButton.isEnabled).to(beFalse())
                    }
                }
            }
        }
    }
}
