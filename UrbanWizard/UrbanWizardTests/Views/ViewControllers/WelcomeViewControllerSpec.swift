//
//  WelcomeViewControllerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 11/18/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class WelcomeViewControllerSpec: QuickSpec {
    override func spec() {
        describe("WelcomeViewController") {
            var subject: WelcomeViewController!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Signup", bundle: nil)
                subject = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                
                subject.view.layoutSubviews()
            }
            
            describe("viewDidLoad") {
                context("content") {
                    it("sign up button should match") {
                        expect(subject.signUpButton.titleLabel!.text).to(equal("Sign Up Now"))
                    }
                    
                    it("login button should match") {
                        expect(subject.logInButton.titleLabel!.text).to(equal("or Log In"))
                    }
                }
            }
        }
    }
}
