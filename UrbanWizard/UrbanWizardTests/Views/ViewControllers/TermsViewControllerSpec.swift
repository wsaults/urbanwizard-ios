//
//  TermsViewControllerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 11/28/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class TermsViewControllerSpec: QuickSpec {
    override func spec() {
        describe("TermsViewController") {
            var subject: TermsViewController!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Signup", bundle: nil)
                subject = storyboard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
                
                subject.view.layoutSubviews()
            }
            
            describe("viewDidLoad") {
                context("content") {
                    it("agree button text should match") {
                        expect(subject.agreeButton.titleLabel?.text).to(equal("I agree"))
                    }
                }
            }
        }
    }
}
