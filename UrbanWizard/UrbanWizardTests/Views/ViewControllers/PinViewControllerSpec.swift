//
//  PinViewControllerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/5/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class PinViewControllerSpec: QuickSpec {
    override func spec() {
        describe("PinViewController") {
            var subject: PinViewController!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                subject = storyboard.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController
                
                subject.view.layoutSubviews()
            }
            
            describe("viewDidLoad") {
                context("content") {
                    it("Enter your pin label should match") {
                        expect(subject.viewModel.pinLabel.text).to(equal("Enter your PIN"))
                    }
                    
                    it("One button should match") {
                        expect(subject.viewModel.oneButton.titleLabel?.text).to(equal("1"))
                    }
                    
                    it("Two button should match") {
                        expect(subject.viewModel.twoButton.titleLabel?.text).to(equal("2"))
                    }
                    
                    it("Three button should match") {
                        expect(subject.viewModel.threeButton.titleLabel?.text).to(equal("3"))
                    }
                    
                    it("Four button should mtach") {
                        expect(subject.viewModel.fourButton.titleLabel?.text).to(equal("4"))
                    }
                    
                    it("Five button should match") {
                        expect(subject.viewModel.fiveButton.titleLabel?.text).to(equal("5"))
                    }
                    
                    it("Six button should match") {
                        expect(subject.viewModel.sixButton.titleLabel?.text).to(equal("6"))
                    }
                    
                    it("Seven button should match") {
                        expect(subject.viewModel.sevenButton.titleLabel?.text).to(equal("7"))
                    }
                    
                    it("Eight button should match") {
                        expect(subject.viewModel.eightButton.titleLabel?.text).to(equal("8"))
                    }
                    
                    it("Nine button should match") {
                        expect(subject.viewModel.nineButton.titleLabel?.text).to(equal("9"))
                    }
                    
                    it("Zero button should match") {
                        expect(subject.viewModel.zeroButton.titleLabel?.text).to(equal("0"))
                    }
                    
                    it("Auth button should match") {
                        expect(subject.viewModel.deviceAuthButton.titleLabel?.text).to(equal("TouchID"))
                    }
                    
                    it("Delete button should match") {
                        expect(subject.viewModel.deleteButton.titleLabel?.text).to(equal("Delete"))
                    }
                }
            }
        }
    }
}
