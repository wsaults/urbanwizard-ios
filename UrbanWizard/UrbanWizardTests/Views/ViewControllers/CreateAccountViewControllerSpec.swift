//
//  CreateAccountViewControllerSpec.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 11/28/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit
import CoreData

@testable import UrbanWizard

class CreateAccountViewControllerSpec: QuickSpec {
    override func spec() {
        describe("CreateAccountViewController") {
            var subject: CreateAccountViewController!
            var mockAlertPresenter: MockAlertPresenter!
            var mockCoreDataStack: MockCoreDataStack!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Signup", bundle: nil)
                subject = storyboard.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
                mockAlertPresenter = MockAlertPresenter()
                subject.alertPresenter = mockAlertPresenter
                
                mockCoreDataStack = MockCoreDataStack()
                subject.coreDataStack = mockCoreDataStack
                subject.view.layoutSubviews()
            }
            
            describe("viewDidLoad") {
                context("content") {
                    it("email placeholder should match") {
                        expect(subject.viewModel.emailView.textField.placeholder).to(equal("Email Address"))
                    }
                    
                    it("password placeholder should match") {
                        expect(subject.viewModel.passwordView.textField.placeholder).to(equal("Password"))
                    }
                    
                    it("password confirmation placeholder should match") {
                        expect(subject.viewModel.confirmPasswordView.textField.placeholder).to(equal("Confirm Password"))
                    }
                    
                    it("continue button text should match") {
                        expect(subject.viewModel.createAccountButton.titleLabel?.text).to(equal("Continue"))
                    }
                }
                
                context("setup") {
                    it("email field should be empty") {
                        expect(subject.viewModel.emailView.textField.text).to(beEmpty())
                    }
                    
                    it("password field should be empty") {
                        expect(subject.viewModel.passwordView.textField.text).to(beEmpty())
                    }
                    
                    it("password confirmation field should be empty") {
                        expect(subject.viewModel.confirmPasswordView.textField.text).to(beEmpty())
                    }
                    
                    it("done button is disabled") {
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("viewModel.coreDataStack should not be nil") {
                        expect(subject.viewModel.coreDataStack).toNot(beNil())
                    }
                }
            }
            
            describe("createAccountButtonTapped") {                
                describe("createAccount - POST Account") {
                    var mockClientManager: MockHTTPClientManager!
                    var mockCoreDataStack: MockCoreDataStack!
                    var mockSession: MockSession!
                    var emailField: UITextField!
                    var passwordField: UITextField!
                    var confirmPasswordField: UITextField!
                    
                    beforeEach {
                        mockClientManager = MockHTTPClientManager()
                        
                        mockCoreDataStack = MockCoreDataStack()
                        subject.viewModel.setup(coreDataStack: mockCoreDataStack)
                        subject.viewModel.clientManager = mockClientManager
                        
                        mockSession = MockSession()
                        subject.viewModel.session = mockSession
                        
                        emailField = UITextField()
                        passwordField = UITextField()
                        confirmPasswordField = UITextField()
                        
                        emailField.text = "test@example.com"
                        passwordField.text = "Password1!"
                        confirmPasswordField.text = "Password1!"
                        
                        subject.viewModel.emailView.textField = emailField
                        subject.viewModel.passwordView.textField = passwordField
                        subject.viewModel.confirmPasswordView.textField = confirmPasswordField
                        
                        mockClientManager.nextAccountResult = Result.success(["account": ["email": "test@example.com"], "token": "1234"])
                    }
                    
                    it("session's auth token should match") {
                        subject.viewModel.createAccount(forViewController: subject) {}
                        
                        expect(mockSession.authToken).to(equal("1234"))
                    }
                    
                    it("should be saved into coredata") {
                        subject.viewModel.createAccount(forViewController: subject) {}
                        
                        var count = 0
                        do {
                            let fetchRequest : NSFetchRequest<Account> = Account.fetchRequest()
                            count = try mockCoreDataStack.mainContext.count(for: fetchRequest)
                        } catch let error as NSError {
                            print("Error \(error), \(error.userInfo)")
                        }
                        
                        expect(count).toEventually(equal(1), timeout: 1)
                    }
                }
            }
            
            describe("textFieldDidChange") {
                context("everything but email is filled out") {
                    beforeEach {
                        subject.viewModel.emailView.textField.text = ""
                        subject.viewModel.passwordView.textField.text = "Password1!"
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!"
                    }
                    
                    it("createAccountButton should become enabled when email is valid") {
                        subject.viewModel.emailView.textField.text = "test@example.com"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beTrue())
                    }
                    
                    it("createAccountButton should become enabled when another email is valid") {
                        subject.viewModel.emailView.textField.text = "test2@example.com"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beTrue())
                    }
                    
                    it("createAccountButton should not become enabled when email is invalid") {
                        subject.viewModel.emailView.textField.text = "invalidEmail"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should become disabled when email is empty") {
                        subject.viewModel.createAccountButton.isEnabled = true
                        subject.viewModel.emailView.textField.text = ""
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("emailView validIndicator color should match when email is valid") {
                        subject.viewModel.emailView.textField.text = "test@example.com"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.emailView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("emailView validIndicator color should match when email is invalid") {
                        subject.viewModel.emailView.textField.text = "invalidEmail"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.emailView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                }
                
                context("everthing but password is filled out") {
                    beforeEach {
                        subject.viewModel.emailView.textField.text = "test@example.com"
                        subject.viewModel.passwordView.textField.text = ""
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!"
                    }
                    
                    it("createAccountButton should become enabled when password is valid") {
                        subject.viewModel.passwordView.textField.text = "Password1!"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beTrue())
                    }
                    
                    it("createAccountButton should become enabled when another password is valid") {
                        subject.viewModel.passwordView.textField.text = "EvenBetterPassword1!"
                        subject.viewModel.confirmPasswordView.textField.text = "EvenBetterPassword1!"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beTrue())
                    }
                    
                    it("createAccountButton should not be enabled when password is invalid") {
                        subject.viewModel.passwordView.textField.text = "lamePassword"
                        subject.viewModel.confirmPasswordView.textField.text = "lamePassword"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccontButton should not be enabled when password is less than 6 characters") {
                        subject.viewModel.passwordView.textField.text = "short"
                        subject.viewModel.confirmPasswordView.textField.text = "short"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should be enabled when valid password is less than 65 characters") {
                        subject.viewModel.passwordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v"
                        subject.viewModel.confirmPasswordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beTrue())
                    }
                    
                    it("createAccountButton should not be enabled when password is more than 64 characters") {
                        subject.viewModel.passwordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v1"
                        subject.viewModel.confirmPasswordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v1"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("passwordView validIndicator color should match when password is greater than 64 characters") {
                        subject.viewModel.passwordView.textField.text = "QAh>NYYn]Qg3ZzY,uMvcvHm,p}dc%sd$pEvtJ{;wrEQxCKxsxjmj8N9PcmgHXD)v1"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.passwordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("createAccountButton should not be enabled when password is missing a capital letter") {
                        subject.viewModel.passwordView.textField.text = "longenoughbutnocapital"
                        subject.viewModel.confirmPasswordView.textField.text = "longenoughbutnocapital"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password is missing a lower case letter") {
                        subject.viewModel.passwordView.textField.text = "ALLCAPS"
                        subject.viewModel.confirmPasswordView.textField.text = "ALLCAPS"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password is missing a number") {
                        subject.viewModel.passwordView.textField.text = "IHaveNoNumber"
                        subject.viewModel.confirmPasswordView.textField.text = "IHaveNoNumber"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password is missing a special character") {
                        subject.viewModel.passwordView.textField.text = "NoSymbol99"
                        subject.viewModel.confirmPasswordView.textField.text = "NoSymbol99"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password contains a '\'") {
                        subject.viewModel.passwordView.textField.text = "Password1!\\"
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!\\"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password contains a '`'") {
                        subject.viewModel.passwordView.textField.text = "Password1!`"
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!`"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password contains a ' '") {
                        subject.viewModel.passwordView.textField.text = "Password1! "
                        subject.viewModel.confirmPasswordView.textField.text = "Password1! "
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password contains a '") {
                        subject.viewModel.passwordView.textField.text = "Password1!'"
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!'"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("createAccountButton should not be enabled when password contains a \"") {
                        subject.viewModel.passwordView.textField.text = "Password1!\""
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!\""
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("passwordView validIndicator background color should match when password contains an forbidden symbol") {
                        subject.viewModel.passwordView.textField.text = "Password1!`"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.passwordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("createAccountButton should become disabled when password is empty") {
                        subject.viewModel.createAccountButton.isEnabled = true
                        subject.viewModel.passwordView.textField.text = ""
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                }
                
                context("everything but confirm password is filled out") {
                    beforeEach {
                        subject.viewModel.emailView.textField.text = "test@example.com"
                        subject.viewModel.passwordView.textField.text = "Password1!"
                        subject.viewModel.confirmPasswordView.textField.text = ""
                    }
                    
                    it("createAccountButton should become enabled when confirm password matches password") {
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beTrue())
                    }
                    
                    it("createAccountButton shoud not be enabled when confirm password does not match password") {
                        subject.viewModel.confirmPasswordView.textField.text = "lamePassword"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                    
                    it("conformPasswordView validIndicator background color should match when passwords match") {
                        subject.viewModel.confirmPasswordView.textField.text = "Password1!"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.confirmPasswordView.validIndicator.backgroundColor).to(equal(UIColor.green))
                    }
                    
                    it("confirmPasswordView validIndicator background color should match when passwords do not match") {
                        subject.viewModel.confirmPasswordView.textField.text = "lamePassword"
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.confirmPasswordView.validIndicator.backgroundColor).to(equal(UIColor.red))
                    }
                    
                    it("createAccountButton should become disabled when confirm password is empty") {
                        subject.viewModel.createAccountButton.isEnabled = true
                        subject.viewModel.confirmPasswordView.textField.text = ""
                        subject.textFieldDidChange()
                        
                        expect(subject.viewModel.createAccountButton.isEnabled).to(beFalse())
                    }
                }
            }
        }
    }
}
