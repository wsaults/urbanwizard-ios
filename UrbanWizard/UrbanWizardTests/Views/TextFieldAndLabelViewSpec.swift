//
//  TextFieldAndLabelView.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/6/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Quick
import Nimble
import UIKit

@testable import UrbanWizard

class TextFieldAndLabelViewSpec: QuickSpec {
    override func spec() {
        describe("TextFieldAndLabelView") {
            var subject: TextFieldAndLabelView!
            let mockLabel = UILabel()
            let mockTextField = UITextField()
            let mockIndicator = UIView()
            let mockWidthConstraint = NSLayoutConstraint()
            
            beforeEach {
                subject = TextFieldAndLabelView()
                
                mockLabel.text = ""
                subject.label = mockLabel
                
                mockTextField.placeholder = "Test Placeholder"
                mockTextField.text = ""
                subject.textField = mockTextField
                
                mockIndicator.backgroundColor = UIColor.gray
                subject.validIndicator = mockIndicator
                
                mockWidthConstraint.constant = 100.0
                subject.widthConstraint = mockWidthConstraint
                
                subject.awakeFromNib()
            }
            
            describe("awakeFromNib()") {
                it("maxWidth should match") {
                    expect(subject.maxWidth).to(equal(100.0))
                }
            }
            
            describe("textDidChange") {
                it("label should be populated with placeholder text") {
                    subject.textField.text = "New text"
                    
                    subject.textDidChange(sender: subject.textField)
                    
                    expect(subject.label.text).to(equal("Test Placeholder"))
                }
                
                it("label should not be populated when textfield is empty")  {
                    subject.textField.text = ""
                    
                    subject.textDidChange(sender: subject.textField)
                    
                    expect(subject.label.text).to(equal(""))
                }
            }
            
            describe("isValid") {
                it("validIndicator color should match when valid") {
                    subject.isValid(true)
                    
                    expect(subject.validIndicator.backgroundColor).to(equal(UIColor.green))
                }
                
                it("validIndicator color should match when invalid") {
                    subject.isValid(false)
                    
                    expect(subject.validIndicator.backgroundColor).to(equal(UIColor.red))
                }
                
                it("validIndicator width should be 100%") {
                    subject.widthConstraint.constant = 50.0
                    subject.isValid(true)
                    
                    expect(subject.widthConstraint.constant).to(equal(100.0))
                }
            }
            
            describe("valid percent") {
                it("validIndicator color should match when valid") {
                    subject.valid(0.0)
                    
                    expect(subject.validIndicator.backgroundColor).to(equal(UIColor.green))
                }
                
                it("validIndicator width should match 25%") {
                    subject.valid(0.25)
                    
                    expect(subject.widthConstraint.constant).to(equal(25.0))
                }
                
                it("validIndicator width should match 50%") {
                    subject.valid(0.5)
                    
                    expect(subject.widthConstraint.constant).to(equal(50.0))
                }
                
                it("validIndicator width should match 75%") {
                    subject.valid(0.25)
                    subject.valid(0.50)
                    subject.valid(0.75)
                    
                    expect(subject.widthConstraint.constant).to(equal(75.0))
                }
                
                it("validIndicator width should match 50%") {
                    subject.valid(0.25)
                    subject.valid(0.50)
                    subject.valid(0.75)
                    subject.valid(0.50)
                    
                    expect(subject.widthConstraint.constant).to(equal(50.0))
                }
            }
        }
    }
}

class SecureTextFieldAndLabelViewSpec: QuickSpec {
    override func spec() {
        describe("SecureTextFieldAndLabelView") {
            var subject: SecureTextFieldAndLabelView!
            let mockTextField = UITextField()
            let mockToggleSecureEntryButton = UIButton()
            
            beforeEach {
                subject = SecureTextFieldAndLabelView()
                
                mockTextField.text = "Some secure text"
                subject.textField = mockTextField
                subject.toggleSecureEntryButton = mockToggleSecureEntryButton
            }
            
            describe("toggleSecureEntry") {
                it("isSecureEntry becomes disabled") {
                    subject.textField.isSecureTextEntry = true
                    
                    subject.toggleSecureEntry()
                    
                    expect(subject.textField.isSecureTextEntry).to(beFalse())
                }
                
                it("isSecureEntry becomes enabled") {
                    subject.textField.isSecureTextEntry = false
                    
                    subject.toggleSecureEntry()
                    
                    expect(subject.textField.isSecureTextEntry).to(beTrue())
                }
            }
        }
    }
}
