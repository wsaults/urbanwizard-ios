//
//  MockHTTPClient.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import Freddy

@testable import UrbanWizard

class MockHTTPClient: HTTPClient {
    var sentURL: String?
    var sentBody: JSON?
    var sentCompletionHandler: DataTaskResult?
    var shouldRundCompletionImmeditaly = true
    var nextResponseData: Data?
    var nextResponse: URLResponse?
    var nextError: Error?
    
    override func post(url: URL, body: JSON?, completion: @escaping DataTaskResult) {
        sentURL = url.absoluteString
        sentBody = body
        sentCompletionHandler = completion
        if shouldRundCompletionImmeditaly {
            completion(nextResponseData, nextResponse, nextError)
        }
    }
    
    override func put(url: URL, body: JSON?, completion: @escaping DataTaskResult) {
        sentURL = url.absoluteString
        sentBody = body
        sentCompletionHandler = completion
        if shouldRundCompletionImmeditaly {
            completion(nextResponseData, nextResponse, nextError)
        }
    }
    
    override func get(url: URL, completion: @escaping DataTaskResult) {
        sentURL = url.absoluteString
        sentCompletionHandler = completion
        if shouldRundCompletionImmeditaly {
            completion(nextResponseData, nextResponse, nextError)
        }
    }
    
    override func delete(url: URL, completion: @escaping DataTaskResult) {
        sentURL = url.absoluteString
        sentCompletionHandler = completion
        if shouldRundCompletionImmeditaly {
            completion(nextResponseData, nextResponse, nextError)
        }
    }
}
