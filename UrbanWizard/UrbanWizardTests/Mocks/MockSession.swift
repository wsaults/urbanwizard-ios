//
//  MockSession.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/2/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
@testable import UrbanWizard

class MockSession: Session {
    var mockDidClear = false
    
    override func clear() {
        mockDidClear = true
    }
}
