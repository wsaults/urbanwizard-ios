//
//  MockHTTPClientManager.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/3/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
@testable import UrbanWizard

class MockHTTPClientManager: HTTPClientManager {
    var nextAccountResult: ResultStringAny = Result.failure(ResponseError.networkUnavailable)
    var nextDictionaryResult: ResultStringObject = Result.failure(ResponseError.networkUnavailable)
    var nextCoinResult: ResultJSON = Result.failure(ResponseError.networkUnavailable)
    
    // MARK: GET
    
    override func readTickerFor(coin: String, completion: @escaping (ResultJSON) -> Void) {
        completion(nextCoinResult)
    }
    
    // MARK: POST
    
    override func createAccount(email: String, password: String, confirmPassword: String, completion: @escaping (ResultStringAny) -> Void) {
        completion(nextAccountResult)
    }
    
    override func createSession(email: String, password: String, completion: @escaping (ResultStringObject) -> Void) {
        completion(nextDictionaryResult)
    }
    
    // MARK: PUT
    
    // MARK: DELETE
}
