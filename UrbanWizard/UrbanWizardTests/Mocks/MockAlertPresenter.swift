//
//  MockAlertPresenter.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/3/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import UIKit
@testable import UrbanWizard

class MockAlertPresenter: AlertPresenting {
    var mockDidPresent = false
    var mockHandler: (() -> Void)?
    var lastTitlePresented: String?
    var lastMessagePresented: String?
    
    func present(_ title: String, message: String?, actionButtonTitle: String?, cancelButtonTitle: String?, handler: (() -> Void)?, onTopOf presenter: UIViewController) {
        mockDidPresent = true
        lastTitlePresented = title
        lastMessagePresented = message
        mockHandler = handler
    }
}
