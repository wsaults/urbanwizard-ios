//
//  MockCoreDataStack.swift
//  UrbanWizardTests
//
//  Created by Will Saults on 12/20/17.
//  Copyright © 2017 AppVentures. All rights reserved.
//

import Foundation
import CoreData

@testable import UrbanWizard

class MockCoreDataStack: CoreDataStack {
    override init() {
        super.init()
        
        let persistentStoreDescription = NSPersistentStoreDescription()
        persistentStoreDescription.type = NSInMemoryStoreType
        
        let container = NSPersistentContainer(name: modelName)
        container.persistentStoreDescriptions = [persistentStoreDescription]
        
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        
        storeContainer = container
    }
}
