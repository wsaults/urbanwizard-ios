# UrbanWizard's README

## Build Setup

``` bash

# install carthage
brew install carthage

# From the project directory
carthage update --platform iOS
```
